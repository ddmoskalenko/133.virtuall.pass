﻿using System;
using System.Collections.Generic;
using System.IO;
using VirtualPass.CE4.Core;
using System.Threading.Tasks;

namespace VirtualPass.CE4.Services
{
    public interface IImageProvider {
        string CroppedExt { get; }

        Task<Stream> Download(string bookingID, string fileName);
        Task<Stream> DownloadMillers(string bookingID, string fileName);
        Task<Stream> DownloadExactlyFrom(string fileName);
        Task<Stream> DownloadPreview(string bookingID, string fileName, int media);
        Task<Stream> Download(string fileName);
        void UploadPreview(string bookingID, string fileName, Stream st);
        void UploadPreview(string bookingID, string fileName, string size, Stream st);
        void UploadOrinal(string bookingID, string fileName, Stream st);
        Stream DownloadOrinal(string bookingID, string fileName, int dpi, out string error);
        bool ExistPreview(string bookingID, string fileName);
        void DeletePreview(string bookingID, string fileName);
        bool MovePreview(int oldBookingID, string filepath, int newBookingID);

        void UploadCrop(string bookingID, string fileName, Stream st);
        int UnzipPreview(string fileName, string bookingID, string imageID);
        void UploadPreviewZip(string fileName, Stream st);
        string GetOriginal(int bookingID, string filename, int dpi = 0);
        string GetOriginalUrl(int bookingID, string fileName);
        string GetPreviewUrl(int bookingID, string fileName, int media);
        string GetUrl(string key);
        void Delete(string fileName);
        void Upload(string key, Stream zipstream,string content);
    }
}
