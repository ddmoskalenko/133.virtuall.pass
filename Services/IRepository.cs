﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VirtualPass.CE4.Models.Cart;
using VirtualPass.CE4.Models.ManageViewModels;
using VirtualPass.CE4.Models;
using NewCoreDll;
using VirtualPass.CE4.Helpers;

namespace VirtualPass.CE4.Services
{
    public interface IRepository
    {
        Task<IList<string>> GetBookings(int employeeID);
        Task<IList<Child>> GetChildrens(Int64 bookingId);
        Task<IList<BookingSitting>> GetSittings(List<string> list);
        Guid? InsertCustomer(Guid bookingSittingGuid, string Email);
        Task<IList<Cart>> GetCart(int cartID);
        //a.s.
        Task<IList<CartItem>> GetCartItems(int cartID);
        Task<IList<ImagePosition>> GetImagePositions(int cartID);
        Task<IList<BookingInfo>> GetBookingInfo(int employeeID);
        Task<IList<CalendarBookingModel>> GetBookingInfoAroundDate(int employeeID, int dateshift);
        Task<IList<RecieptDeteilsPr>> GetReceiptDetails(int orderId);
        DigitalCEContext Context();
        Task<Tuple<Guid, int>> GetCartOrderPlaced(string email, string bsGIUD);
        bool CloseCartSession(int item2);
        decimal UpdateTaxRate(string bookingId, decimal taxRate);

        Task<Booking> GetBooking(int bookingID);
        Task<List<Image>> GetImages(int bookingID);
        Task<RosterChildren> GetChildren(int rosterID);
        bool OpenCartSession(int cartId);
        int GetCartId(string CustomerGUID);

        void SaveImagePosition(int cartID, int imagePosition, int oldImagePosition);
        int InsertCart(CartProcedure myCart);
        void SaveImagePosition(int cartId, IEnumerable<ImagePosition> positions);
        //Task<Customer> GetCustomers(string customerGuid);
        Task<Cart> GetCart(string customerGuid);
        Task<IList<PortraitOptions>> GetPortraitSheetOptions(string customerGuid);

        bool RemoveOrderPlaced(int cartID);
        Task<IList<CartItemTotals>> CartItemTotals(string customerGuid);
        decimal GetTaxRate(string CustomerGUID);
        decimal GetHandlingCharge(string CustomerGUID);
        string GetEmailByCustomerGuid(string CustomerGUID);
        Task<IList<UpsellPromptsPr>> GetUpsellPromptsByCustomerGuid(string customerGuid);
        Task<IList<RewardsByCustomerPr>> GetRewardsByCustomerGuid(string customerGuid);
        Task<List<Image>> GetAllImages();


        Task<IList<CustomerSummaryDataPr>> GetCustomerSummaryData(int cartID);

        object ExecuteScalar(string procedureName, Dictionary<string, object> parameters, DigitalCEContext context);

        int InsertCashOrderCustomer(OrderDatails od);
        bool DeleteOrderIfExist(string CustomerGUID);

        Task<IList<States>> GetStates_DDL();
        Task<IList<CardTypes>> GetCardType_DDL();

        Task<IList<SpecialtyImagesPr>> GetSpecialtyImages(string customerGuid);
        Task<IList<CartItemPr>> CartItemsPrByCartId(int cartId);
        Task UpdateOpenSession(int cartID);
        Task<IList<SheetCountPricingPr>> GetSheetCountPricingByCustomerGUID(string customerGuid);

        int DeleteCartItems(int CartID, decimal Price, string Type);
        int InsertCartItems(List<CartItem> myCartItemList);
        int UpdateCartCatalogPricing(int cartId, string discount);
        void InsertOrderPaymentOffline(OrderPaymentDetails opd, int cartID);
        void UpdateOrderStatus(int orderId, string orderStatus);

        void SetBookingStatus(int bookingID, BookingStatus status);
        Task<IList<Image>> GetBookingImages(int bookingID, int employeeid);
        // Task<IList<int>> GetUnTransferedImages(int bookingID, int employeeid, string computer);
        IEnumerable<PasserDetailsModel> GetPasserDetails(string bookingID);
        GetPDModel GetPricingStructure(string bookingID);
        Task<IList<int>> GetCartProducts(int cartID);
        Task<IList<PaymentDetailsPr>> GetReceiptPaymentDetails(int orderId);
        Task<IList<PriceStructuresPr>> GetPriceStructuresActiveList();
        int UpdatePricingStructureID(int bookingId, int pricingID);
        IDictionary<string, IEnumerable<dynamic>> GetPasserAdminReport();

        Task<IList<PayTypePr>> GetPayTypesActiveList();
        Task<IList<DiscountTypePr>> GetDiscountTypeActiveList();

        int GetOrderIDByCartID(int CartID);
        IList<OrderItem> GetOrderItems(int OrderID);
        Task<IList<OrderDetailsPr>> GetOrderDetails(int OrderID);
        List<int> GetDependedSitting(int parentbookingsittingid);
        int UpdateCustomer(Guid customerGuid, string firstName, string lastName, string address1, string address2, string city, int stateId, string zip, string Phone);
        int UpdateOrderDiscount(int orderId, decimal discountAmount, int discountType, decimal total, decimal tax);
        int InsertHoldOrder(OrderDatails od);
        int UpdateOrderAuthorized(int orderId, bool authorized);
        OnlinePaymantSystem.Step1Result UpdateOrderAuthorization(string transID, OrderDatails od, int bookingID);
    }
}
