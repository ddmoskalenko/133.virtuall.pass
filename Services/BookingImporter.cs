﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NewCoreDll;
using VirtualPass.CE4.Models;

namespace VirtualPass.CE4.Services
{
    public class BookingImporter
    {
        private IConfigurationRoot configuration;
        private IRepository repository;
        private IServiceProvider provider;
        readonly UserManager<ApplicationUser> userManager;
        readonly SignInManager<ApplicationUser> signInManager;

        private string ApiNsiUrl;
        private string Bearer;

        public BookingImporter(IServiceProvider provider)
        {
            configuration = provider.GetService<IConfigurationRoot>();
            userManager = provider.GetService<UserManager<ApplicationUser>>(); 
            this.ApiNsiUrl = configuration.GetConnectionString("ApiNSISUrl");
            this.Bearer = configuration["ApiNSISBearer"];
            repository = provider.GetService<IRepository>();
            this.provider = provider;
        }

        public async Task<System.IO.Stream> Import(int size,string url)
        {
            System.IO.Stream retValue = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ApiNsiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Bearer);
                try
                {
                    HttpResponseMessage response = await client.GetAsync(url);
                    if (response.IsSuccessStatusCode)  retValue = await response.Content.ReadAsStreamAsync();
                }
                catch(Exception e) { string s = e.Message;  }
            }
            return retValue;
        }

        public async Task<int> Process(int employee = 0)
        {// employee=0 import all bookings
            using (var db = repository.Context())
            {
                int? downloadlogid = db.BookingDownloadsLog.Max(s => s.BookingDowloadID);
                if ((downloadlogid != null) && (downloadlogid > 0))
                {
                    var downloadlog = db.BookingDownloadsLog.Where(k => k.BookingDowloadID == downloadlogid).FirstOrDefault();
                    if (downloadlog.IsFinished == false)
                    {
                        if ((DateTime.Now - downloadlog.Started).Minutes < 10) return 0;
                        downloadlog.IsFinished = true;
                        db.SaveChanges();
                    }
                }
                var nlog = new BookingDownloadLog();
                nlog.Started = DateTime.Now;
                db.BookingDownloadsLog.Add(nlog);
                db.SaveChanges();
                int nlogid = nlog.BookingDowloadID;

                int pageSize = 50;
                int antiloop = 5;
                bool hasnew = true;
                int page = 0;
                DateTime dtstart = DateTime.Now;
                while (antiloop > 0 && hasnew && DateTime.Compare(dtstart, DateTime.Now) < 300)
                {
                    System.IO.Stream stream = await Import(pageSize, "/api/bookings?id="+employee+"&page="+page);
                    page++;
                    antiloop--;
                    if (stream == null) { hasnew = false; }
                    string result = System.Text.Encoding.UTF8.GetString((stream as System.IO.MemoryStream).ToArray());
                    BookingModelNSIS j = JsonConvert.DeserializeObject<BookingModelNSIS>(result);
                    if (j.Results != null)
                    {
                        try
                        {
                            List<BookingHash> gotBookHashs = j.Results.ToList();
                            List<int> gotIDs = gotBookHashs.Select(p => p.BookingID).ToList();

                            var db2 = provider.GetService<IRepository>().Context();
                            var ourBookHashs = db2.BookingInfo.Where(b => gotIDs.Contains(b.BookingID)).Select(d => new BookingHash()
                            {
                                BookingID = d.BookingID,
                                Hash = d.Hash
                            }).ToList();
                            //
                            var foundnotchanged = gotBookHashs.Where(i => ourBookHashs.Any(b => b.BookingID == i.BookingID && b.Hash == i.Hash)).ToList();
                            var foundchanged = gotBookHashs.Where(i => ourBookHashs.Any(b => b.BookingID == i.BookingID && b.Hash != i.Hash)).ToList();

                            var found = foundnotchanged.Concat(foundchanged).ToList();
                            var notfound = gotBookHashs
                                          .Where(p2 => !found
                                          .Any(p1 => p1.BookingID == p2.BookingID))
                                          .ToList();
                            if (notfound.Count == 0)
                            {
                                Console.WriteLine("Nothing to import");
                                hasnew = false;
                            }

                            foreach (BookingHash bh in notfound) await ImportOneBooking(bh);
                            foreach (BookingHash bh in foundchanged) await UpdateOneBooking(bh);

                            var nlog2 = db2.BookingDownloadsLog.Where(k => k.BookingDowloadID == nlogid).FirstOrDefault();
                            nlog2.Inserted = nlog2.Inserted + notfound.Count();
                            nlog2.Updated = nlog2.Updated + foundchanged.Count();
                            nlog2.IsFinished = true;
                            db2.SaveChanges();

                        }
                        catch (Exception e)
                        {
                            string ee = e.Message;
                            nlog.IsFinished = true;
                            db.SaveChanges();
                            return 3;
                        }
                    }
                }
            }
            
            CheckUsers(100);
            return 1;
        }

        public async void CheckUsers(int pagesize)
        {
            System.IO.Stream stream = await Import(pagesize, "/api/bookings/user");
            if (stream == null) return;
            string result = System.Text.Encoding.UTF8.GetString((stream as System.IO.MemoryStream).ToArray());
            PersonsModel model = JsonConvert.DeserializeObject<PersonsModel>(result);
            if (model.Results != null)
            {
                foreach (Person p in model.Results)
                {
                    try
                    {
                        var user = new ApplicationUser { UserName = p.Email, Email = p.Email };
                        var resulta = await userManager.CreateAsync(user, p.Password);
                        await userManager.AddClaimAsync(user, new Claim("EmployeeID", p.EmployeeID.ToString()));
                        //
                        using (var client = new HttpClient())
                        {
                            client.BaseAddress = new Uri(ApiNsiUrl);
                            client.DefaultRequestHeaders.Accept.Clear();
                            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Bearer);
                            try
                            {
                                HttpResponseMessage response = await client.GetAsync("/ api/bookings/markasimported?username="+ p.Email);
                                await response.Content.ReadAsStreamAsync();
                            }
                            catch (Exception e) { string s = e.Message; }
                        }
                    }
                    catch { }
                }
            }
        }

        public async Task<int> UpdateOneBooking(BookingHash bh)
        {
            Console.Write(".");
            Stream retValue = await GetBookingInfio(bh.BookingID);
            if (retValue != null)
            {
                try
                {
                    string result = System.Text.Encoding.UTF8.GetString((retValue as System.IO.MemoryStream).ToArray());
                    BookingGeneral model = JsonConvert.DeserializeObject<BookingGeneral>(result);
                    //
                    if (model != null)
                    {
                        var db3 = provider.GetService<IRepository>().Context();
                        Booking bc = db3.Booking.Where(b => b.BookingID == bh.BookingID).FirstOrDefault();
                        if (bc == null) return 11;
                        bc.PricingStructureID = model.PromotionID;
                        db3.SaveChanges();

                        BookingInfo bi = db3.BookingInfo.Where(b => b.BookingID == bh.BookingID).FirstOrDefault();
                        if (bi == null) return 12;
                        bi.BookingNotes = model.BookingNotes;
                        bi.GiftID = model.GiftID;
                        bi.ShootDate = model.ShootDate;
                        bi.TaxRate = model.TaxRate;
                        bi.BookingNotes = model.Notes;
                        bi.GiftID = model.GiftID;
                        bi.AccountID = model.AccountID;
                        bi.BookerID = model.BookerID;
                        bi.ShooterID = model.ShooterID;
                        bi.PasserID = model.PasserID;
                        bi.Hash = bh.Hash;
                        db3.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    logger.LogError(e, "Error in booking");
                    return 10;
                }
            }
            return 0;
        }

        public async Task<System.IO.Stream> GetBookingInfio(int bookingID)
        {
            Console.Write(".");
            Stream ret = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ApiNsiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Bearer);
                HttpResponseMessage response = await client.GetAsync("/api/bookings/one?id=" + bookingID);
                if (response.IsSuccessStatusCode) ret = await response.Content.ReadAsStreamAsync();
            }
            return ret;
        }

        public async Task<int> ImportOneBooking(BookingHash bh)
        {
            int b1 = 0;
            int b2 = 0;
            System.IO.Stream retValue = await GetBookingInfio(bh.BookingID);
            if (retValue != null)
            {
                try
                {
                    string result = System.Text.Encoding.UTF8.GetString((retValue as System.IO.MemoryStream).ToArray());
                    BookingGeneral model = JsonConvert.DeserializeObject<BookingGeneral>(result);
                    if (model != null)
                    {

                        Booking bc = new Booking();
                        bc.BookingID = model.BookingID;
                        bc.PricingStructureID = model.PromotionID;
                        bc.PassDate = model.PassDate;
                        var db3 = provider.GetService<IRepository>().Context();
                        db3.Booking.Add(bc);
                        db3.SaveChanges();
                        b1++;

                        BookingInfo bi = new BookingInfo();
                        bi.BookingID = model.BookingID;
                        bi.BookingNotes = model.BookingNotes;
                        bi.GiftID = model.GiftID;
                        bi.ShootDate = model.ShootDate;
                        bi.TaxRate = model.TaxRate;
                        bi.BookingNotes = model.Notes;
                        bi.GiftID = model.GiftID;
                        bi.AccountID = model.AccountID;
                        bi.BookerID = model.BookerID;
                        bi.ShooterID = model.ShooterID;
                        bi.PasserID = model.PasserID;
                        bi.Hash = bh.Hash;
                        db3.BookingInfo.Add(bi);
                        db3.SaveChanges();
                        b2++;
                    }
                }
                catch (Exception e)
                {
                    logger.LogError(e,"Import booking failed");
                }
            }
            return b1 + b2;
        }
    }
}
