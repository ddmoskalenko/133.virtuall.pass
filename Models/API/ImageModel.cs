﻿using System;
using System.Collections.Generic;

namespace VirtualPass.CE4.Models
{
    public class ImageModel
    {
        public int ImageID { get; set; }
        public int? Pose { get; set; }
        public Dictionary<int, string> sizes = new Dictionary<int, string>();

        public ImageModel(int imageID,int? pose)
        {
            ImageID = imageID;
            Pose = pose;
        }
    }
}
