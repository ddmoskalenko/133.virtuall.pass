﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualPass.CE4.Models
{
    public class CustomerModel
    {
        public int CustomerID { get; set; }
        public int? BookingSittingID { get; set; } 
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CustomerGUID { get; set; }
        public string Email { get; set; }
    }
}
