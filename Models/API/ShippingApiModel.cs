﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualPass.CE4.Models
{
    public class ShippingApiModel
    {
        public int ShippingID { get; set; }
        public string AccessCode { get; set; }
        public string TrackingNumber { get; set; }
        public string Carrier { get; set; }
        public string ShippingDate { get; set; }
        public string ShippingMethod { get; set; }
        public int? OrderID { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }

    }
}
