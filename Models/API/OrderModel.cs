﻿using System.Collections.Generic;

namespace VirtualPass.CE4.Models
{
    public class OrderModel
    {
        public int OrderID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Email { get; set; }
        public decimal? Subtotal { get; set; }
        public decimal? Tax { get; set; }
        public decimal? Shipping { get; set; }
        public decimal? Handling { get; set; }
        public decimal? DiscountAmount { get; set; }
        public decimal? Total { get; set; }
        public string AccessCode { get; set; }
        public string Phone { get; set; }
        public List<OrderItemModel> items = new List<OrderItemModel>();
    }
}
