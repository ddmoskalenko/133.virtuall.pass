﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualPass.CE4.Models
{
    public class OrderItemModel
    {
        public int OrderItemID { get; set; }
        public int? OrderID { get; set; }
        public string ProductID { get; set; }
        public int?[] ImageID { get; set; }
        public int? Quantity { get; set; }
        public decimal? Price { get; set; }
    }
}
