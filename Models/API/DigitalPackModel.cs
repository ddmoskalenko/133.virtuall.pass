﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualPass.CE4.Models
{
    public class DigitalPackModel
    {
        public string Zip { get; set; }
        public string AccessCode { get; set; }
        public int ImagesCount { get; set; }
        public string Message { get; set; }

        public List<string> PhotoPathList { get; set; }

        public DigitalPackModel(string accesscode)
        {
            AccessCode = accesscode;
            PhotoPathList = new List<string>();
        }
    }
}
