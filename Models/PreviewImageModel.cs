﻿using System.Collections.Generic;

namespace VirtualPass.CE4.Models
{
    public class PreviewImageModel
    {
        public string MyCustomerGUID { get; set; }
        public int MyCustomerCartId { get; set; }
        public string MyBookingGuid { get; set; }
        

        public int ID { get; set; }
        public string ImagePath { get; set; }
        public List<string> previews;
        public int BookingID { get; set; }
        public int? Pose { get; set; }

        public PreviewImageModel(int IDe, string ipe, int bookingID )
        {
            ID = IDe;
            ImagePath = ipe;
            BookingID = bookingID;
            previews = new List<string>();
        }

        public PreviewImageModel(int IDe, string ipe, int bookingID,int? pose): this(IDe,ipe,bookingID)
        {
            Pose = pose;
        }
    }

}
