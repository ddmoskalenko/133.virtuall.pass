﻿using System;
using System.Collections.Generic;

namespace VirtualPass.CE4.Models
{
    public class BookingGeneral
    {
        public int BookingID { get; set; }
        public DateTime? ShootDate { get; set; }
        public string BookingNotes { get; set; }
        public string PassNotes { get; set; }
        public string AccountName { get; set; }
        public int? GiftID { get; set; }
        public decimal? TaxRate { get; set; }
        public int PromotionID { get; set; }
        public int AccountID { get; set; }
        public int ContactID { get; set; }
        public int BookerID { get; set; }
        public int ShooterID { get; set; }
        public int PasserID { get; set; }
        public int TeamLeadID { get; set; }
        public bool? Approved { get; set; }
        public DateTime BookedDate { get; set; }
        public DateTime PassDate { get; set; }
        public bool IsCancelled { get; set; }
        public bool IsMakeup { get; set; }
        public bool IsPostponed { get; set; }
        public bool Active { get; set; }
        public int? CompPasserID { get; set; }
        public int? HoldPasserID { get; set; }
        public int? AdditionalGiftID { get; set; }
        public int? EstimatedSets { get; set; }
        public int? ActualSets { get; set; }
        public string Notes { get; set; }
        public string CompType { get; set; }
    }
}
