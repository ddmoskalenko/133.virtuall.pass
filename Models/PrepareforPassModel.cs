﻿using NewCoreDll;

namespace VirtualPass.CE4.Models
{
    public class PrepareforPassModel
    {
        public PrepareforPassModel(string iLocalBridgeUrl)
        {
            LocalBridgeUrl = iLocalBridgeUrl;
        }

        public string LocalBridgeUrl { get; }
        public BookingSitting sitting { get; set; }
        public RosterChildren children { get; set; }
    }
}
