﻿using NewCoreDll;
using System;
using System.Collections.Generic;

namespace VirtualPass.CE4.Models
{
    public class BookingModel : BookingInfo
    {
        public int PricingStructureID { get; set; }
        public string PricingStructureName { get; set; }
        public string Tax { get; set; }
    }

    public class BookingApiModel
    {
        public int BookingID { get; set; }
        public IList<ChildApiModel> childs;
        public DateTime? ShootDate { get; set; }
        public string TaxRate { get; set; }
    }
}
