﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace VirtualPass.CE4.Models
{
    public class BookingHash
    {   [Key]
        public int BookingID { get; set; }
        public string Hash { get; set; }
    }
}
