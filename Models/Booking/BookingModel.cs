﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace VirtualPass.CE4.Models
{
    [DataContract]
    public class BookingModelNSIS
    {
        [DataMember]
        public List<BookingHash> Results { get; set; }
        [DataMember]
        public int TotalCount { get; set; }
        [DataMember]
        public int TotalPages { get; set; }
        [DataMember]
        public string PrevPageLink { get; set; }
        [DataMember]
        public string NextPageLink { get; set; }
    }
}
       