﻿using NewCoreDll;
using System.Collections.Generic;

namespace VirtualPass.CE4.Models
{
    public class CoverBookingModel
    {
        public IList<BookingInfo> TableOfBookings { get; set; }
        public IList<CalendarBookingModel> CandarOfBookings { get; set; }
        public int EmployeeID { get; set; }
    }
}
