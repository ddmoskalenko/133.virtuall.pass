﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VirtualPass.CE4.Services;
using VirtualPass.CE4.Core;
using System.Data.SqlClient;
using System.Data;
using VirtualPass.CE4.Models;
using VirtualPass.CE4.Models.ManageViewModels;
using VirtualPass.CE4.Models.Cart;
using NewCoreDll;
using System.Globalization;
using VirtualPass.CE4.Helpers;

namespace VirtualPass.Core.Reps
{
    public class Repository : IRepository
    {
        DigitalCEContext digitalCEContext;

        public Repository(DigitalCEContext digitalCEContext)
        {
            this.digitalCEContext = digitalCEContext;
        }

        public DigitalCEContext Context()
        {
            return digitalCEContext;
        }


        public Guid? InsertCustomer(Guid bookingSittingGuid, string Email)
        {
            var result = ExecuteScalar("InsertCustomer", new Dictionary<string, object>()
            {
                { "Email", Email },
                    { "BookingSittingGUID", bookingSittingGuid }
            }, digitalCEContext);
            return result != null ? (Guid?)Guid.Parse(result.ToString()) : null;

        }


        public Task<IList<string>> GetBookings(int employeeID)
        {
            /*
            return Task.Factory.StartNew<IList<string>>(() =>
            {
                IList<string> result = digitalCEContext.BookingInfo.Where(s => s.Status != null && (s.Status.Value == BookingStatus.Ready || s.Status.Value == BookingStatus.InProgress)).Select(s => s.BookingID.ToString()).ToList();
                return result;
            });
            */
            return Task.Factory.StartNew<IList<string>>(() =>
            {
                IList<string> result = digitalCEContext.BookingInfo.Where(s => s.Status != null && (s.Status.Value == BookingStatus.Ready || s.Status.Value == BookingStatus.InProgress && (s.BookerID == employeeID || s.PasserID == employeeID || s.ShooterID == employeeID))).Select(s => s.BookingID.ToString()).ToList();
                return result;
            });
        }

        public List<int> GetDependedSitting(int parentbookingsittingid)
        {
            return (digitalCEContext
                .SittingLinks
                .Where(s => s.ParentBookingSittingID == parentbookingsittingid)
                .Select(s => s.BookingSittingID)).ToList();
        }

        public Task<IList<Image>> GetBookingImages(int bookingID, int employeeid)
        {
            return Task.Factory.StartNew<IList<Image>>(() =>
            {
                return (digitalCEContext
                    .Images
                    .Where(b => b.BookingID == bookingID && b.ShooterID == employeeid && b.ImageStatusID != (int)ImageStatusK.ShoulbeDeletedInHotFolder && b.ImageStatusID != (int)ImageStatusK.Downloading)
                    .OrderBy(b => b.Pose)
                    .ThenByDescending(b => b.LoadDate)).ToList();
            });
        }
      
    }
}
