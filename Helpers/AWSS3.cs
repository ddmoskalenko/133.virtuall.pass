﻿using System;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon;
using Amazon.S3.Transfer;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace VirtualPass.CE4.Helpers
{
    public class AWSS3 : IStorage
    {
        readonly string bucketName;
        readonly IAmazonS3 client;
        readonly ILogger logger;

        public AWSS3()
        {
        }

        public AWSS3(IConfigurationRoot appConfiguration, ILoggerFactory loggerFactory)
        {
            logger = loggerFactory.CreateLogger<AWSS3>();
            try
            {
                string ff = appConfiguration["AccessKey"];
                bucketName = appConfiguration["BucketName"];
                client = new AmazonS3Client(appConfiguration["AccessKey"], appConfiguration["SecretAccessKey"], RegionEndpoint.USEast1);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }
        }

        Stream ReadStream(Stream responseStream)
        {
            byte[] buffer = new byte[16 * 1024];
            MemoryStream ms = new MemoryStream();
            int read;
            while ((read = responseStream.Read(buffer, 0, buffer.Length)) > 0)
            {
                ms.Write(buffer, 0, read);
            }
            ms.Position = 0;
            return ms;
        }

        public IList<string> GetAllKeys()
        {
            IList<string> result = new List<string>();
            try
            {
                ListObjectsRequest request = new ListObjectsRequest
                {
                    BucketName = bucketName,
                    MaxKeys = 2
                };

                do
                {
                    ListObjectsResponse response = client.ListObjectsAsync(request).Result;
                    foreach (S3Object entry in response.S3Objects)
                    {
                        result.Add(entry.Key);
                    }
                    if (response.IsTruncated)
                    {
                        request.Marker = response.NextMarker;
                    }
                    else
                    {
                        request = null;
                    }
                } while (request != null);
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    logger.LogError("Check the provided AWS Credentials.");
                    logger.LogError("To sign up for service, go to http://aws.amazon.com/s3");
                }
                else
                {
                    logger.LogError("Error occurred. Message:'{0}' when listing objects", amazonS3Exception.Message);
                }
            }
            return result;
        }

        public void Rename(string a1, string a2)
        {
            CopyObjectRequest copyRequest = new CopyObjectRequest();
            copyRequest.SourceBucket = bucketName;
            copyRequest.SourceKey = a1;
            copyRequest.DestinationBucket = bucketName;
            copyRequest.DestinationKey = a2;
            copyRequest.CannedACL = S3CannedACL.PublicRead;
            client.CopyObjectAsync(copyRequest);

            DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
            deleteRequest.BucketName = bucketName;
            deleteRequest.Key = a1;
            client.DeleteObjectAsync(bucketName, a1);
        }

        public void CreateFile(string path, Stream st, string content = "image/jpeg")
        {
            if (Exists(path))
            {
                Delete(path);
            }
            TransferUtility fileTransferUtility = new TransferUtility(client);
            TransferUtilityUploadRequest fileTransferUtilityRequest = new TransferUtilityUploadRequest
            {
                BucketName = bucketName,
                Key = path,
                InputStream = st,
                ContentType = content,
                AutoResetStreamPosition = true,
                AutoCloseStream = true
            };
            fileTransferUtilityRequest.CannedACL = S3CannedACL.PublicRead;
            fileTransferUtility.Upload(fileTransferUtilityRequest);
        }

        public async void CreateFile2(string key, Stream ms, string content)
        {
            try
            {
                PutObjectRequest putObjectRequestFolder = new PutObjectRequest();
                putObjectRequestFolder.BucketName = bucketName;
                putObjectRequestFolder.Key = key;
                putObjectRequestFolder.ContentType = content;
                putObjectRequestFolder.InputStream = ms;
                PutObjectResponse or = await client.PutObjectAsync(putObjectRequestFolder);
            }
            catch (Exception ee) { string n = ee.Message; }
            int jj = 9;
        }

        public void Delete(string fromfile)
        {
            DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
            {
                BucketName = bucketName,
                Key = fromfile
            };
            var result = client.DeleteObjectAsync(deleteObjectRequest).Result;
        }

        public Stream Download(string path)
        {
            GetObjectRequest request = new GetObjectRequest
            {
                BucketName = bucketName,
                Key = path
            };
            using (GetObjectResponse response = client.GetObjectAsync(request).Result)
            {
                return ReadStream(response.ResponseStream);
            }
        }

        public string Url(string path)
        {
            GetPreSignedUrlRequest request = new GetPreSignedUrlRequest();
            request.BucketName = bucketName;
            request.Key = path;

            request.Expires = DateTime.Now.Add(new TimeSpan(7, 0, 0, 0));
            return client.GetPreSignedURL(request);
        }

        public Stream DownloadFile(string path)
        {
            return Download(path);
        }

        public bool Exists(string dpath)
        {
            GetObjectRequest request = new GetObjectRequest
            {
                BucketName = bucketName,
                Key = dpath,
            };
            try
            {
                using (GetObjectResponse response = client.GetObjectAsync(request).Result)
                {
                    return response != null && response.ResponseStream != null && response.ResponseStream.Length > 0;
                }
            }
            catch
            {
                return false;
            }
        }

        public string[] GetDirectories(string path)
        {
            IList<string> keys = GetAllKeys()
                .Where(s => s.Contains(path))
                .ToList();
            StringHeler helper = new StringHeler();
            return helper.GetArray(keys, path);
        }

        public void Move(string fileName, string v)
        {
            Stream df = DownloadFile(fileName);
            Delete(fileName);
            CreateFile(v, df);
        }
    }
}
