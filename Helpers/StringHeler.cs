﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualPass.CE4.Helpers
{
    public class StringHeler
    {
        public string[] GetArray(IList<string> keys, string path) {
            IList<string> result = new List<string>();
            foreach (string p in keys) {
                string n = p.Replace(path, "");
                if (!string.IsNullOrEmpty(n) && n != "\\") {
                    if (n[0] == '\\') {
                        n = n.Substring(1, n.Length - 1);
                    }
                    string item = n.Split('\\')[0];
                    if (!string.IsNullOrEmpty(item) && item != "\\" && !item.Contains('.')) {
                        result.Add(item.Replace("\\", ""));
                    }
                }
            }
            return result.ToArray();
        }
    }
}
