﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualPass.CE4.Helpers
{
    public class CookiesHelper
    {
        static volatile CookiesHelper instance;
        static object syncRoot = new Object();

        public static CookiesHelper Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new CookiesHelper();
                    }
                }
                return instance;
            }
        }
        public Dictionary<string, string> CookiesDict { get; set; }

        public CookiesHelper()
        {
            CookiesDict = new Dictionary<string, string>();
        }
    }
}
