﻿using System;
using System.Collections.Generic;
using System.IO;

namespace VirtualPass.CE4.Helpers
{
    public interface IStorage
    {
        Stream Download(string path);
        string Url(string path);
        string[] GetDirectories(string path);
        bool Exists(string dpath);
        void CreateDirectory(string path);
        void CreateFile(string path, Stream st, string content = "image/jpeg");
        Stream DownloadFile(string path);
        void Delete(string fromfile);
        void Move(string fileName, string v);

        void Rename(string a1, string a2);
        IList<string> GetAllKeys();
    }
}
