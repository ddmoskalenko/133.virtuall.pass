﻿using NewCoreDll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualPass.CE4.Services;

namespace VirtualPass.CE4.Helpers
{
    public class Agent
    {
        private int cartID;
        readonly IRepository repository;

        public Agent(string cartID, IRepository repository)
        {
            this.repository = repository;
            this.cartID = int.Parse(cartID);
        }

        internal bool TryCorrectCartItems()
        {
            int orderID = repository.GetOrderIDByCartID(cartID);
            IList<OrderItem> listOrderItems = repository.GetOrderItems(orderID);
            if (listOrderItems == null || listOrderItems.Count() == 0)
            {
                return false;
            }
            List<CartItem> myCartItemList = new List<CartItem>();
            foreach (OrderItem item in listOrderItems)
            {
                CartItem ci = new CartItem();
                ci.CartID = cartID;
                ci.ImageID = item.ImageID ?? -1;
                ci.ProductID = item.ProductID ?? 0;
                ci.Quantity = item.Quantity ?? 0;
                ci.Price = item.Price ?? 0;
                ci.PriceWithReward = item.Price ?? 0;
                ci.PriceWithoutReward = item.Price ?? 0;
                myCartItemList.Add(ci);
            }
            repository.InsertCartItems(myCartItemList);

            return true;
        }
    }
}
