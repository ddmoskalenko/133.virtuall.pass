﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualPass.CE4.Helpers
{
    public class Converter
    {
        NumberFormatInfo nfi;
        static volatile Converter instance;
        static object syncRoot = new Object();

        public static Converter Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new Converter();
                    }
                }
                return instance;
            }
        }

        private Converter()
        {
            nfi = new NumberFormatInfo() { NumberDecimalSeparator = "." };
        }

        public decimal ConverToDecimal(string value)
        {
            return decimal.Parse(value, nfi);
        }
    }
}
