﻿using System;
using System.ComponentModel;

namespace VirtualPass.CE4.Helpers.Authorize
{
    [Serializable]
    internal class AuthNetAccountInfo : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        private string _version = String.Empty;
        
        /// <summary>
        /// The version of Authorize.Net you wish to target.
        /// </summary>
        public string AuthNetVersion
        {
            get { return _version; }
            set
            {
                _version = value;
                OnPropertyChanged("AuthNetVersion");
            }
        }

        private string _loginID = String.Empty;
        /// <summary>
        /// The Login ID represents your unique Authorize.Net account.
        /// </summary>
        public string AuthNetLoginID
        {
            get { return _loginID; }
            set
            {
                _loginID = value;
                OnPropertyChanged("AuthNetLoginID");
            }
        }

        private string _transKey = String.Empty;
        /// <summary>
        /// Acquired from Authorize.Net, its use replaces the old User ID and Password paradigm.
        /// </summary>
        public string AuthNetTransKey
        {
            get { return _transKey; }
            set
            {
                _transKey = value;
                OnPropertyChanged("AuthNetTransKey");
            }
        }

        private bool _isTestMode;
        /// <summary>
        /// Set this to True for Test transactions and False for live ones.
        /// This allows you to test individual transactions as opposed to placing
        /// the entire Account in Test Mode.
        /// </summary>
        public bool IsTestMode
        {
            get { return _isTestMode; }
            set
            {
                _isTestMode = value;
                OnPropertyChanged("IsTestMode");
            }
        }
    }
}
