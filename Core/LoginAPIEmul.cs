﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualPass.CE4.Core
{
    public struct UserInfo
    {
        public bool IsExist;
        public int EmployeeID;
    }

    public interface ILoginAPIEmul
    {
        UserInfo GetUserInfo(string userName, string password);
    }
}
