﻿using Microsoft.Extensions.Logging;
using System;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using VirtualPass.CE4.Helpers;
using VirtualPass.CE4.Services;

namespace VirtualPass.CE4.Core
{
    public class ImageProvider : IImageProvider
    {
        readonly ILogger logger;
        readonly IStorage storage;
        IConfigurationRoot appConfiguration;

        public ImageProvider(ILoggerFactory loggerFactory, IStorage storage, IConfigurationRoot appConfiguration)
        {
            this.storage = storage;
            logger = loggerFactory.CreateLogger<ImageProvider>();
            this.appConfiguration = appConfiguration;
        }

        public static string cropped = ".cropped.jpg";
        public static string root = @"C:\Images\";
        public static string preview = "Preview";
        public static string original = "Original";

        public string CroppedExt
        {
            get
            {
                return cropped;
            }
        }

        public Task<Stream> Download(string bookingID, string fileName)
        {
            return Task.Factory.StartNew<Stream>(() =>
            {
                string path = Path.Combine(root, bookingID, fileName);
                try
                {
                    return storage.Download(path);
                }
                catch (Exception ex)
                {
                    string f = ex.Message;
                }
                return null;
            });
        }

        public Task<Stream> DownloadExactlyFrom(string fileName)
        {
            return Task.Factory.StartNew<Stream>(() =>
            {
                if (storage.Exists(fileName))
                    try
                    {
                        return storage.Download(fileName);
                    }
                    catch (Exception ex)
                    {
                        string f = ex.Message;
                    }
                return null;
            });
        }

        public Task<Stream> Download(string fileName)
        {
            return Task.Factory.StartNew<Stream>(() =>
            {
                string p = root + fileName;
                if (storage.Exists(p))
                    return storage.Download(p);
                else return null;
            });
        }

        public string GetOriginal(int bookingID, string filename, int dpi = 0)
        {
            string opath = Path.Combine(root, bookingID.ToString());
            string path = Path.Combine(opath, original);
            if (dpi == 0)
                return Path.Combine(path, filename);
            string ppath = Path.Combine(opath, preview, dpi.ToString());
            return Path.Combine(ppath, filename + cropped);
        }

        public bool ExistPreview(string bookingID, string fileName)
        {
            string p = Path.Combine(root, bookingID, preview, "150", fileName + cropped);
            return storage.Exists(p);
        }

        public Task<Stream> DownloadPreview(string bookingID, string fileName, int media)
        {
            if (fileName == null)
                return null;

            return Task.Factory.StartNew<Stream>(() =>
            {
                string mp = Path.Combine(root, bookingID, preview, media.ToString(), fileName);
                return storage.Download(mp);
            });
        }

        public void UploadOrinal(string bookingID, string fileName, Stream st)
        {
            string path = Path.Combine(root, bookingID, original, fileName);
            try
            {
                storage.CreateFile(path, st);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }
        }

        public Stream DownloadOrinal(string bookingID, string fileName, int dpi, out string error)
        {
            string path = string.Empty;
            if (dpi > 0)
                path = Path.Combine(root, bookingID, original, dpi.ToString(), fileName);
            else
                path = Path.Combine(root, bookingID, original, fileName);
            try
            {
                if (storage.Exists(path))
                {
                    error = string.Empty;
                    return storage.DownloadFile(path);
                }
                else
                {
                    error = "File not found";
                    return null;
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            return null;
        }

        public void DeletePreview(string bookingID, string fileName)
        {
            string error = "";
            string path = Path.Combine(root, bookingID, preview);
            foreach (string frommediadirectory in storage.GetDirectories(path))
            {
                string fromfile = Path.Combine(path, frommediadirectory, fileName);
                try
                {
                    storage.Delete(fromfile);
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
        }

        public void Delete(string fileName)
        {
            string error = "";
            try
            {
                storage.Delete(fileName);
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
        }


        public void UploadPreview(string bookingID, string fileName, Stream st)
        {
            string path = Path.Combine(root, bookingID.ToString(), fileName);
            try
            {
                storage.CreateFile(path, st);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }
        }

        public void UploadPreview(string bookingID, string fileName, string size, Stream st)
        {
            string path = Path.Combine(root, bookingID.ToString(), preview, size.ToString(), fileName);
            try
            {
                storage.CreateFile(path, st);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }
        }

        public int UnzipPreview(string fileName, string bookingID, string imageID)
        {
            string path = Path.Combine(root, bookingID, preview);
            Stream fz = storage.Download(fileName);
            using (var zip = new ZipArchive(fz, ZipArchiveMode.Read))
            {
                foreach (var entry in zip.Entries)
                {
                    using (var stream = entry.Open())
                    {
                        string zpath = Path.Combine(path, entry.Name);
                        storage.CreateFile(zpath, stream);
                    }
                }
            }
            storage.Move(fileName, Path.Combine(path, preview, imageID + ".zip"));
            return 0;
        }

        public void UploadPreviewZip(string fileName, Stream st)
        {
            string path = Path.GetTempPath() + "//" + fileName;
            try
            {
                storage.CreateFile(path, st);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }
        }

        public void UploadCrop(string bookingID, string fileName, Stream st)
        {
            string path = root;
            if (!storage.Exists(path))
                storage.CreateDirectory(path);
            path = Path.Combine(root, bookingID, fileName);
            try
            {
                storage.CreateFile(path, st);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }
        }

        public bool MovePreview(int oldBookingID, string imgPath, int newBookingID)
        {
            string frompath = Path.Combine(root, oldBookingID.ToString(), preview);
            string topath = Path.Combine(root, newBookingID.ToString(), preview);
            string justfile = Path.GetFileName(imgPath);

            List<string> sizesAll = appConfiguration["CroppsSizes"].Split(',').ToList();
            sizesAll.AddRange(appConfiguration["PreviewSizes"].Split(','));
            string[] sizes = sizesAll.ToArray();
            foreach (string frommediadirectory in sizes)
            {
                string fromfile = Path.Combine(frompath, frommediadirectory, justfile);
                string tofile = Path.Combine(topath, frommediadirectory, justfile);
                try
                {
                    storage.Move(fromfile, tofile);
                }
                catch (Exception ex) { logger.LogError("Moving is not successful"); }
            }
            return true;
        }

        public string GetPreviewUrl(int bookingID, string fileName, int media)
        {
            return storage.Url(Path.Combine(Path.Combine(root, bookingID.ToString() + @"\" + preview + @"\" + media.ToString(), fileName)));
        }

        public string GetOriginalUrl(int bookingID, string fileName)
        {
            return storage.Url(Path.Combine(Path.Combine(root, bookingID.ToString() + @"\" + original + @"\", fileName)));
        }

        public void Upload(string key, Stream zipstream, string content)
        {
            storage.CreateFile(key, zipstream, content);
        }

        public string GetUrl(string key)
        {
            return storage.Url(key);
        }
    }
}
