﻿using System;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.Extensions.Configuration;

namespace VirtualPass.CE4.Core
{
    public class AuthorizeIPAddressAttribute : ActionFilterAttribute
    {
        IConfigurationRoot appConfiguration;
        public AuthorizeIPAddressAttribute(IConfigurationRoot appConfiguration)
        {
            this.appConfiguration = appConfiguration;
          
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            string ipAddress = context.HttpContext.Connection.RemoteIpAddress.ToString();
            if (!IsIpAddressAllowed(ipAddress.Trim()))
            {
                context.Result = new JsonResult("Unauthorized IP address ["+ ipAddress + "]");
            }
            base.OnActionExecuting(context);
        }

        private bool IsIpAddressAllowed(string IpAddress)
        {
            if (!string.IsNullOrWhiteSpace(IpAddress))
            {
                var addresses = appConfiguration.GetSection("AllowedIPAddresses").GetChildren().Select(x => x.Value).ToList(); ;
                return addresses.Where(a => a.Trim().Equals(IpAddress, StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            return false;
        }
    }
}
