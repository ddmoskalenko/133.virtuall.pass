﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using VirtualPass.CE4.Models.ManageViewModels;

namespace VirtualPass.CE4.Core
{
    public static class UiHelper
    {
        public static List<ListItemModel> CCExpYear()
        {
            List<ListItemModel> ddl = new List<ListItemModel>();
            ListItemModel li99 = new ListItemModel();
            li99.Text = "Select Year";
            li99.Value = "";
            ddl.Add(li99);

            int CurYear = DateTime.Now.Year;
            int TargetYear = CurYear + 10;

            for (int i = CurYear; i <= TargetYear; i++)
            {
                ListItemModel li = new ListItemModel();
                li.Text = i.ToString();
                li.Value = i.ToString();

                ddl.Add(li);
            }
            return ddl;
        }

        public static List<ListItemModel> CCExpMonth()
        {
            List<ListItemModel> ddl = new List<ListItemModel>();
            ListItemModel li98 = new ListItemModel();
            li98.Text = "Select Month";
            li98.Value = "";

            ddl.Add(li98);
            var cult = new System.Globalization.CultureInfo("en-US");
            for (int j = 1; j <= 12; j++)
            {
                string monthName = cult.DateTimeFormat.GetMonthName(j);
                ListItemModel li = new ListItemModel();
                li.Text = j.ToString("00") + " - " + monthName;
                li.Value = j.ToString();

                ddl.Add(li);
            }
            return ddl;
        }
    }
}
