﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualPass.CE4.Core
{
    public class ImagePathVariables
    {
        public string PageAddress { get; set; }
        public string PageName { get; set; }
        public string PageLocation { get; set; }
        public string QueryString { get; set; }
    }
}
