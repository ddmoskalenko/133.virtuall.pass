﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;

namespace VirtualPass.CE4.Core
{
    /// <summary>
    /// Long Running Test Class.
    /// </summary>
    public class MyLongRunningClass
    {

        private static object syncRoot = new object();

        /// <summary>
        /// Gets or sets the process status.
        /// </summary>
        /// <value>The process status.</value>
        private static IDictionary<string, int> ProcessStatus { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MyLongRunningClass"/> class.
        /// </summary>
        public MyLongRunningClass()
        {
            if (ProcessStatus == null)
            {
                ProcessStatus = new Dictionary<string, int>();
            }
        }

        /// <summary>
        /// Processes the long running action.
        /// </summary>
        /// <param name="id">The id.</param>
        public string ProcessLongRunningAction(string id)
        {
            for (int i = 1; i <= 100; i++)
            {
                Thread.Sleep(100);
                lock (syncRoot)
                {
                    ProcessStatus[id] = i;
                }
            }
            return id;
        }

        /// <summary>
        /// Adds the specified id.
        /// </summary>
        /// <param name="id">The id.</param>
        public bool Add(string id)
        {
            lock (syncRoot)
            {
                if (!ProcessStatus.ContainsKey(id))
                {
                    ProcessStatus.Add(id, 0);
                    return true;
                }
            }
            return false;
     
        }

        /// <summary>
        /// Removes the specified id.
        /// </summary>
        /// <param name="id">The id.</param>
        public void Remove(string id)
        {
            lock (syncRoot)
            {
                ProcessStatus.Remove(id);
            }
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <param name="id">The id.</param>
        public int GetStatus(string id)
        {
            lock (syncRoot)
            {
                if (ProcessStatus.Keys.Count(x => x == id) == 1)
                {
                    return ProcessStatus[id];
                }
                else
                {
                    return 100;
                }
            }
        }
    }
}

