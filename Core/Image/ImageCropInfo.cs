﻿using System.Runtime.Serialization;


namespace VirtualPass.CE4.Core
{
    public class ImageCropInfo
    {
        public double autocropsourcex { get; set; }
        public double autocropsourcey { get; set; }
        public double autocropwidthpercentage { get; set; }
        public double autocropheightpercentage { get; set; }
        public int rotation { get; set; }
        public int imageid { get; set; }
        public string pose { get; set; }
        public string previewsizes { get; set; }
        public int bookingid { get; set; }
        public string filename { get; set; }
    }
}


