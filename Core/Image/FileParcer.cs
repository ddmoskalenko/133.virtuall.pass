﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace VirtualPass.CE4.Core
{
    [DataContract]
    public class FileNameParces
    {
        public string FileName { get; set; }
        public string BookingID { get; set; }
        public int ImageID { get; set; }

        public FileNameParces(string path)
        {
            ImageID = 0;
            if ((string.IsNullOrEmpty(path)) || (path.IndexOf("\\") < 0)) return;
            FileName = path.Substring(path.LastIndexOf("\\") + 1);

            if (FileName.ToLower().IndexOf("preview") < 0)
            {
                try
                {
                    ImageID = Convert.ToInt32(FileName.Remove(FileName.LastIndexOf(".")));
                    BookingID = "0";
                }
                catch { }
            }
            else
            {
                string pacc = path.Remove(path.LastIndexOf("\\"));
                try
                {
                    ImageID = Convert.ToInt32(pacc.Substring(pacc.LastIndexOf("\\") + 1));
                }
                catch { }
                pacc = pacc.Remove(pacc.LastIndexOf("\\"));
                BookingID = pacc.Substring(pacc.LastIndexOf("\\") + 1);
            }
        }
        public bool Confly()
        {
            return ((!string.IsNullOrEmpty(BookingID)) && (ImageID != 0) && (!string.IsNullOrEmpty(FileName)));
        }
    }

    [DataContract]
    public class FileNameParcesCollection
    {
        [DataMember]
        public int totalcount { get; set; }
        [DataMember]
        public int bookingid { get; set; }
        [DataMember]
        public FileNameParces[] mem { get; set; }
        [DataMember]
        public int completeness { get; set; }
    }
}
