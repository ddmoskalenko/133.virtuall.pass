﻿using System;
using System.Runtime.Serialization;

namespace VirtualPass.CE4.Core
{ 
    [DataContract]
    public class PhotoFiles
    {
        [DataMember]
        public PhotoFile[] photos { get; set; }
    }
}
