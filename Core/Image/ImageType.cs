﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualPass.CE4.Core
{

    public enum ImageType
    {
        Preview = 1,
        Product = 2
    }

    public enum ImageStatusK
    {
        UnAttached = 4,
        TransferStart = 1,
        TransferComplete = 2,
        MillersIngestComplete = 3,
        ShoulbeDeletedInHotFolder=5,
        Downloading = 6,
    }
}
