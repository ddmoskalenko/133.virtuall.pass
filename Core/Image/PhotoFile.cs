﻿using System;
using System.Runtime.Serialization;
using System.Security.Cryptography;

namespace VirtualPass.CE4.Core
{
    [DataContract]
    public class PhotoFile
    {
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string Ext { get; set; }
        [DataMember]
        public string FileNameWithoutExt { get; set; }
        [DataMember]
        public string Dir { get; set; }
        [DataMember]
        public string ComputerName { get; set; }
        [DataMember]
        public int ImageID { get; set; }
        [DataMember]
        public Int64 FileSize { get; set; }
        [DataMember]
        public string Hash { get; set; }
        [DataMember]
        public int ShooterID { get; set; }
        [DataMember]
        public int BookingID { get; set; }

        public PhotoFile()
        {
          
        }

       

      
    }
}
