﻿using System;

namespace VirtualPass.CE4.Core.Image
{
    public enum PreviewSizes
    {
        croppsSizes150 = 150,
        croppsSizes300 = 300,
        croppsSizes500 = 500,
        croppsSizes800 = 800,
        croppsSizes1200 = 1200,
    }
}
