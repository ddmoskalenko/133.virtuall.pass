﻿using System.Runtime.Serialization;


namespace VirtualPass.CE4.Core
{
    [DataContract]
    public class ChildInfo
    {
        [DataMember]
        public string childfirstname;
        [DataMember]
        public string childlastname;
        [DataMember]
        public string classname;
        [DataMember]
        public string teacherName;
        [DataMember]
        public int rosterid;
        [DataMember]
        public string customfield1;
        [DataMember]
        public string customfield2;
        [DataMember]
        public int? imageid;
        [DataMember]
        public string classroom;
        [DataMember]
        public string student;
        [DataMember]
        public bool director;
        [DataMember]
        public bool staff;
        [DataMember]
        public string accesscode;
        [DataMember]
        public int? family;
        [DataMember]
        public int? bookingsittingid;
    }
}
