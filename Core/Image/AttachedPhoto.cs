﻿using System;
using System.Runtime.Serialization;
using System.Security.Cryptography;

namespace VirtualPass.CE4.Core
{
    [DataContract]
    public class AttachedPhoto
    {
        [DataMember]
        public int ImageID { get; set; }
        [DataMember]
        public int BookingSittingID { get; set; }
        [DataMember]
        public int Pose { get; set; }
        [DataMember]
        public int BookingID { get; set; }
    }
}
