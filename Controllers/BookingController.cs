﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualPass.CE4.Core;
using VirtualPass.CE4.Models;
using VirtualPass.CE4.Models.Cart;
using VirtualPass.CE4.Models.ManageViewModels;
using VirtualPass.CE4.Services;
using VirtualPass.CE4.Helpers;
using NewCoreDll;
using System.Globalization;
using Newtonsoft.Json;
using VirtualPass.CE4.Core.SalesPart;
using System.Text.RegularExpressions;


namespace VirtualPass.CE4.Controllers
{
    [Authorize]
    public class BookingController : Controller
    {
        IRepository repository;

        public BookingController(IRepository repository)
        {
            this.repository = repository;
        }

        public async Task<IActionResult> Index()
        {
            IList<string> model = await repository.GetBookings(4641);
            return View(model);
        }


        [HttpGet]
        public async Task<IActionResult> ChildSelection(string bookingId)
        {
            Int64 intBookingId;
            if (string.IsNullOrEmpty(bookingId) || !Int64.TryParse(bookingId, out intBookingId))
            {
                var linkParametrs = ReadCookies();
                bookingId = linkParametrs.BookingID.ToString();
                if (string.IsNullOrEmpty(bookingId) || !Int64.TryParse(bookingId, out intBookingId))
                {
                    return RedirectToAction("Error");
                }
            }

            ChildSelectFormer former = new ChildSelectFormer();
            IList<ChildViewModel> model = await former.GetModel(intBookingId, repository);
            ViewData["BookingID"] = bookingId;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> ChildSelection([FromBody] ChildSelectionSubmitModel model)
        {
            return await PosesReview();
        }


        [HttpPost]
        public async Task<IActionResult> CustomerExist([FromBody] QueryStringValues model)
        {
            string txtEmail = model.Email;
            string bookingGuid = model.BookingSittingGUID;

            var cokiesReader = CookiesHelper.Instance;
            if (cokiesReader.CookiesDict.Count > 0)
            {
                foreach (var item in cokiesReader.CookiesDict)
                {
                    if (item.Key == txtEmail)
                    {
                        var valueMas = item.Value.Split('&');
                        var resCustomerGuid = valueMas[1];
                        var resCartId = Convert.ToInt32(valueMas[3]);

                        CloseSession(resCartId);

                        return Json(item);
                    }
                }
            }

            return Json(null);
        }


        [HttpPost]
        public string SaveLinksParametrs([FromBody] QueryStringValues model)
        {
            string result = "true";
            var response = Request.HttpContext.Response;

            var modelJson = JsonConvert.SerializeObject(model);
            var encryptModelJson = Helpers.Crypt.EncryptStringToBytes_Aes(modelJson);

            try
            {
                Microsoft.AspNetCore.Http.CookieOptions cookOption = new Microsoft.AspNetCore.Http.CookieOptions();
                cookOption.Expires = DateTime.Now.AddDays(1);
                response.Cookies.Append("link", encryptModelJson, cookOption);
            }
            catch (Exception)
            {
                result = "false";
            }

            return result;
        }

        public QueryStringValues ReadCookies()
        {
            string link = "";
            string linkPass = "";


            var сookies = Request.Cookies;
            foreach (var item in сookies)
            {
                if (item.Key == "link")
                {
                    link = item.Value;
                }
            }
            foreach (var item in сookies)
            {
                if (item.Key == "linkPasser")
                {
                    linkPass = item.Value;
                }
            }
            var edecModel = Helpers.Crypt.DecryptStringFromBytes_Aes(linkPass);
            var mod = JsonConvert.DeserializeObject<QueryStringValues>(edecModel);
            if (!string.IsNullOrEmpty(linkPass) && !mod.IsVisited)
            {
                link = linkPass;
            }

            var edecriptModel = Helpers.Crypt.DecryptStringFromBytes_Aes(link);
            var model = JsonConvert.DeserializeObject<QueryStringValues>(edecriptModel);

            if (mod != null)
            {
                if (mod.IsVisited && mod.IsAdmin)
                {
                    model.RedirectToPasser = true;
                }


            }

            return model;
        }


        [HttpPost]
        public async Task<IActionResult> GetCustomer([FromBody] QueryStringValues model)
        {
            string txtEmail = model.Email;
            string bookingGuid = model.BookingSittingGUID;
            bool isFullData = bookingGuid != "" && bookingGuid != "none";
            string customerGUID = null;

            if (isFullData)
            {
                var custGUID = repository.InsertCustomer(Guid.Parse(bookingGuid), txtEmail);
                customerGUID = custGUID.HasValue ? custGUID.Value.ToString() : null;
            }

            return Json(customerGUID);
        }

        [HttpPost]
        public async Task OpenSession([FromBody] int cartID)
        {
            await repository.UpdateOpenSession(cartID);
        }

        public IActionResult Error()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        [HttpPost]
        public async Task<IActionResult> CheckUser(string bookingGuid, string email)
        {
            return await Task.Factory.StartNew<IActionResult>(() =>
            {
                return null;
            });
        }


        public async Task<IActionResult> PosesReview()
        {
            PosesReviewFormer former = new PosesReviewFormer();
            QueryStringValues linkParametrs = ReadCookies();
            if (string.IsNullOrEmpty(linkParametrs.BookingSittingGUID))
            {
                return RedirectToAction("Error");
            }

            IList<PreviewImageModel> model = await former.GetPoses(linkParametrs, repository);

            if (model.Count == 0)
            {
                return RedirectToAction("Error");
            }

            // backward link
            ViewData["BookingID"] = model[0].BookingID;
            ViewData["Query"] = Request.Query;
            return View(model);
        }


        public async Task<IActionResult> Packages()
        {
            PackagesFormer former = new PackagesFormer();
            QueryStringValues linkParametrs = ReadCookies();

            if (string.IsNullOrEmpty(linkParametrs.BookingSittingGUID) || string.IsNullOrEmpty(linkParametrs.CustomerGUID))
            {
                return RedirectToAction("Error");
            }

            PackagesModel model = await former.GetPackages(linkParametrs, repository);
            if (model == null)
            {
                return RedirectToAction("Error");
            }

            ViewData["BookingID"] = model.BookingID;
            return View(model);
        }


        public async Task<IActionResult> DigitalPackages()
        {
            PackagesFormer former = new PackagesFormer();
            QueryStringValues linkParametrs = ReadCookies();

            if (string.IsNullOrEmpty(linkParametrs.CustomerGUID) || string.IsNullOrEmpty(linkParametrs.BookingSittingGUID) || Convert.ToInt32(linkParametrs.CartID) < 1)
            {
                return RedirectToAction("Error");
            }

            TestIsVisited(linkParametrs);
            if (linkParametrs.RedirectToPasser)
            {
                DeletePasserCookies(linkParametrs);
                return RedirectToAction("PasserDetails", "PasserAdmin", new { bookingID = linkParametrs.BookingID.ToString() });
            }

            DigitalPackagesModel model = await former.GetDigitalPackage(linkParametrs, repository);

            if (model == null)
            {
                return RedirectToAction("Error");
            }

            ViewData["BookingID"] = model.BookingID;
            return View(model);
        }



        public async Task<IActionResult> BestValuePackage()
        {
            PackagesFormer former = new PackagesFormer();
            QueryStringValues linkParametrs = ReadCookies();

            if (string.IsNullOrEmpty(linkParametrs.CustomerGUID) || string.IsNullOrEmpty(linkParametrs.BookingSittingGUID) || Convert.ToInt32(linkParametrs.CartID) < 1)
            {
                return RedirectToAction("Error");
            }

            TestIsVisited(linkParametrs);

            if (linkParametrs.RedirectToPasser)
            {
                DeletePasserCookies(linkParametrs);
                return RedirectToAction("PasserDetails", "PasserAdmin", new { bookingID = linkParametrs.BookingID.ToString() });
            }

            BestValuePackageModel model = await former.GetBestValuePackage(linkParametrs, repository);

            if (model == null)
            {
                return RedirectToAction("Error");
            }

            ViewData["BookingID"] = model.BookingID;
            return View(model);
        }


        public async Task<IActionResult> ClassicPackage()
        {
            PackagesFormer former = new PackagesFormer();
            QueryStringValues linkParametrs = ReadCookies();

            if (string.IsNullOrEmpty(linkParametrs.CustomerGUID) || string.IsNullOrEmpty(linkParametrs.BookingSittingGUID) || Convert.ToInt32(linkParametrs.CartID) < 1)
            {
                return RedirectToAction("Error");
            }
            TestIsVisited(linkParametrs);
            if (linkParametrs.RedirectToPasser)
            {
                DeletePasserCookies(linkParametrs);
                return RedirectToAction("PasserDetails", "PasserAdmin", new { bookingID = linkParametrs.BookingID.ToString() });
            }


            ClassicPackageModel model = await former.GetClassicPackage(linkParametrs, repository);

            if (model == null)
            {
                return RedirectToAction("Error");
            }

            ViewData["BookingID"] = model.BookingID;
            return View(model);
        }


        public async Task<IActionResult> StandardPackage()
        {
            QueryStringValues linkParametrs = ReadCookies();

            if (string.IsNullOrEmpty(linkParametrs.CustomerGUID) || string.IsNullOrEmpty(linkParametrs.BookingSittingGUID) || Convert.ToInt32(linkParametrs.CartID) < 1)
            {
                return RedirectToAction("Error");
            }
            TestIsVisited(linkParametrs);
            if (linkParametrs.RedirectToPasser)
            {
                DeletePasserCookies(linkParametrs);
                return RedirectToAction("PasserDetails", "PasserAdmin", new { bookingID = linkParametrs.BookingID.ToString() });
            }

            PackagesFormer former = new PackagesFormer();
            StandardPackageModel model = await former.GetStandardPackage(linkParametrs, repository);

            if (model == null)
            {
                return RedirectToAction("Error");
            }

            ViewData["BookingID"] = model.BookingID;
            return View(model);
        }


        public async Task<IActionResult> SheetSelection()
        {
            QueryStringValues linkParametrs = ReadCookies();

            if (string.IsNullOrEmpty(linkParametrs.CustomerGUID) || string.IsNullOrEmpty(linkParametrs.BookingSittingGUID) || Convert.ToInt32(linkParametrs.CartID) < 0)
            {
                return RedirectToAction("Error");
            }

            SheetSelectionFormer former = new SheetSelectionFormer();
            SheetSelectionModel model = await former.GetSheetSelection(linkParametrs, repository);

            if (model == null)
            {
                return RedirectToAction("Error");
            }

            ViewData["BookingID"] = model.BookingID;
            return View(model);
        }


        public async Task<IActionResult> CartReviewSheetSelection()
        {
            QueryStringValues linkParametrs = ReadCookies();

            if (string.IsNullOrEmpty(linkParametrs.CustomerGUID) || string.IsNullOrEmpty(linkParametrs.BookingSittingGUID) || Convert.ToInt32(linkParametrs.CartID) < 1)
            {
                return RedirectToAction("Error");
            }
            TestIsVisited(linkParametrs);
            if (linkParametrs.RedirectToPasser)
            {
                DeletePasserCookies(linkParametrs);
                return RedirectToAction("PasserDetails", "PasserAdmin", new { bookingID = linkParametrs.BookingID.ToString() });
            }

            SheetSelectionFormer former = new SheetSelectionFormer();
            CartReviewSheetSelectionModel model = await former.GetCartReviewSheetSelection(linkParametrs, repository);

            if (model == null)
            {
                return RedirectToAction("Error");
            }

            ViewData["BookingID"] = model.BookingID;
            return View(model);
        }


        public async Task<IActionResult> CustomerCheckout()
        {
            QueryStringValues linkParametrs = ReadCookies();

            if (string.IsNullOrEmpty(linkParametrs.CustomerGUID) || string.IsNullOrEmpty(linkParametrs.BookingSittingGUID) || Convert.ToInt32(linkParametrs.CartID) < 1)
            {
                return RedirectToAction("Error");
            }

            CustomerCheckoutFormer former = new CustomerCheckoutFormer();
            CustomerCheckoutModel model = await former.GetCustomerCheckout(linkParametrs, repository);

            if (model == null)
            {
                return RedirectToAction("Error");
            }

            ViewData["BookingID"] = model.BookingID;
            return View(model);
        }

        [HttpPost]
        public string SaveImagePositions([FromBody] IList<ImagePosition> data)
        {
            int cartId = data.FirstOrDefault().CartID;
            string customerGuid = data.FirstOrDefault().CustomerGuid;
            string selectPackage = data.FirstOrDefault().SelectPackage;
            string bookingGuid = data.FirstOrDefault().BookingGuid;
            string price = "0,00";
            decimal myprice = 0;
            var cartIdByCustGuid = repository.GetCartId(customerGuid);
            if (cartIdByCustGuid > 0)
            {
                cartId = cartIdByCustGuid;
            }
            //NumberFormatInfo nfi = new NumberFormatInfo() { NumberDecimalSeparator = "." };

            int productId = 0;
            //Get price
            PackagesPrice packPrice = new PackagesPrice();
            packPrice.SetPrice(customerGuid, repository);

            switch (selectPackage)
            {
                case "p-special":
                    price = packPrice.SupSevPrice;
                    productId = 23;
                    break;
                case "classic-package":
                    price = packPrice.ClPacPrice;
                    productId = 21;
                    break;
                case "standard-package":
                    price = packPrice.StPacPrice;
                    productId = 22;
                    break;
                case "d-pack":
                    price = packPrice.DigImPdice;
                    productId = 17;
                    break;
                default:
                    break;
            }

            if (cartId <= 0)
            {
                cartId = -1;

                CartProcedure myCart = new CartProcedure();
                decimal mySubtotal = Converter.Instance.ConverToDecimal(price);

                myCart.CartID = cartId;
                myCart.CustomerGUID = customerGuid;
                myCart.Subtotal = mySubtotal;
                myCart.CatalogDiscount = "false";
                myCart.CatalogDiscountPerc = 0.00M;

                cartId = repository.InsertCart(myCart);
            }
            else
            {
                var resRemove = repository.RemoveOrderPlaced(cartId);
                CloseSession(cartId);


                CartProcedure myCart = new CartProcedure();
                decimal mySubtotal = Converter.Instance.ConverToDecimal(price);

                //decimal mySubtotal = decimal.Parse(price, nfi);

                myCart.CartID = cartId;
                myCart.CustomerGUID = customerGuid;
                myCart.Subtotal = mySubtotal;
                myCart.CatalogDiscount = "false";
                myCart.CatalogDiscountPerc = 0.00M;

                cartId = repository.InsertCart(myCart);
            }
            repository.SaveImagePosition(cartId, data);
            SaveCookie(customerGuid, cartId);

            List<CartItem> myCartItemList = new List<CartItem>();

            CartItem ci = new CartItem();
            ci.CartID = cartId;
            ci.ImageID = -1;
            ci.ProductID = productId;
            ci.Quantity = 1;
            ci.Price = Converter.Instance.ConverToDecimal(price);
            ci.PriceWithReward = Converter.Instance.ConverToDecimal(price);
            ci.PriceWithoutReward = Converter.Instance.ConverToDecimal(price);

            myCartItemList.Add(ci);

            int bookingID = repository.GetSittings((new string[] { bookingGuid.ToString() }).ToList()).Result.FirstOrDefault().BookingID;
            var booking = repository.GetBooking(bookingID).Result;
            int pricingStructureId = int.Parse(booking.PricingStructureID.ToString());
            bool isWinBack20 = pricingStructureId == (int)PricingStructureCodes.WinBack20;
            myprice = Converter.Instance.ConverToDecimal(price);
            if (myCartItemList.Count > 0)
            {
                int myTempCartID = repository.DeleteCartItems(cartId, myprice, "All"); //#ao adding price
                int myTempCartItemSuccess = repository.InsertCartItems(myCartItemList);

            }

            return cartId.ToString();
        }

        [HttpPost]
        public string SavePositionsSheetSelection([FromBody] IList<SheetSelectionExistModel> data)
        {
            int cartId = data.FirstOrDefault().CartID;
            string customerGuid = data.FirstOrDefault().CustomerGuid;
            var subtotal = data.FirstOrDefault().Subtotal;
            string catalogDiscount = data.FirstOrDefault().CatalogDiscount;
            var catalogDiscountPerc = data.FirstOrDefault().CatalogDiscountPerc;

            if (cartId <= 0)
            {
                cartId = -1;

                CartProcedure myCart = new CartProcedure();
                decimal mySubtotal = Convert.ToDecimal(subtotal);

                myCart.CartID = cartId;
                myCart.CustomerGUID = customerGuid;
                myCart.Subtotal = mySubtotal;
                myCart.CatalogDiscount = catalogDiscount;
                myCart.CatalogDiscountPerc = Convert.ToDecimal(catalogDiscountPerc);
                cartId = repository.InsertCart(myCart);
            }
            else
            {
                CloseSession(cartId);
                CartProcedure myCart = new CartProcedure();
                decimal mySubtotal = Convert.ToDecimal(subtotal);

                myCart.CartID = cartId;
                myCart.CustomerGUID = customerGuid;
                myCart.Subtotal = mySubtotal;
                myCart.CatalogDiscount = catalogDiscount;
                myCart.CatalogDiscountPerc = Convert.ToDecimal(catalogDiscountPerc);
                cartId = repository.InsertCart(myCart);
            }

            List<CartItem> myCartItemList = GetSheetQuantities(data, cartId);
            if (myCartItemList.Count > 0)
            {
                int myTempCartID = repository.DeleteCartItems(cartId, subtotal, "Sheets"); //#ao adding price
                int myTempCartItemSuccess = repository.InsertCartItems(myCartItemList);

                int myUPDCartID = repository.UpdateCartCatalogPricing(cartId, catalogDiscount);
            }

            SaveCookie(customerGuid, cartId);

            return cartId.ToString();
        }


        [HttpPost]
        public string CheckCash([FromBody] OrderDatails od)
        {
            List<States> states = repository.GetStates_DDL().Result.ToList();

            int OrderID = -1;
            od.OrderType = "Pass";
            od.CheckoutType = "Customer";
            od.OrderStatus = "Awaiting Payment";
            od.Authorized = false;
            od.Tax = Convert.ToDecimal(od.Tax);
            od.Subtotal = Convert.ToDecimal(od.Subtotal);
            od.Handling = Convert.ToDecimal(od.Handling);
            od.Total = Convert.ToDecimal(od.Total);
            od.StateID = Convert.ToInt32(states.Where(sss => sss.State == od.State).FirstOrDefault().StateID);

            var resDeleteOrderIfExist = repository.DeleteOrderIfExist(od.CustomerGUID);
            OrderID = repository.InsertCashOrderCustomer(od);

            if (od.CustomerGUID != "" && od.CartID != -1)
            {
                CloseSession(od.CartID);
            }

            string email = od.Email;
            var cokiesReader = CookiesHelper.Instance;
            var сookies = Request.Cookies;
            foreach (var item in сookies)
            {

                var keyCookie = item.Key;
                var valueCookie = item.Value;

                if (keyCookie == email)
                {
                    cokiesReader.CookiesDict.Remove(keyCookie);

                    var response = Request.HttpContext.Response;
                    response.Cookies.Delete(email);
                }
            }

            return "true";
        }


        [HttpPost]
        public string ProcessCustomerCCOffline([FromBody] OrderDatails od)
        {
            List<States> states = repository.GetStates_DDL().Result.ToList();
            List<CardTypes> cardTipes = repository.GetCardType_DDL().Result.ToList();

            od.Phone = RegexPhone(od.Phone);

            string mySuccess = "false";
            string AuthNetTransID = "-1";
            int myOrderID = -1;
            od.OrderStatus = "Order Placed";
            od.Authorized = false; //#ao if customer paying with cc, authorized is false
            od.CheckoutType = "Customer"; //#ao Update checkout type to indicate it was processed by Admin
            if (od.CCCreditCardType == "Select Card Type")
            {
                od.CCCreditCardTypeID = -1;
            }
            else
            {
                od.CCCreditCardTypeID = Convert.ToInt32(cardTipes.Where(ci => ci.CardType == od.CCCreditCardType).FirstOrDefault().CardTypeID);
            }
            od.StateID = Convert.ToInt32(states.Where(sss => sss.State == od.State).FirstOrDefault().StateID);
            od.Tax = Convert.ToDecimal(od.Tax);
            od.Subtotal = Convert.ToDecimal(od.Subtotal);
            od.Handling = Convert.ToDecimal(od.Handling);

            //online paymant
            try
            {
                od.CheckNumber = "-1";

                OnlinePaymantSystem paymSyst = new OnlinePaymantSystem();
                OnlinePaymantSystem.Step1Result status = paymSyst.SendUpPayments(od, repository);
                if (status == OnlinePaymantSystem.Step1Result.Complete)
                {
                    mySuccess = "true";
                }
                else
                {
                    mySuccess = "false";
                }
            }
            catch (Exception ex)
            {

                mySuccess = "false";
            }

            if (od.CustomerGUID != "" && od.CartID != -1)
            {
                CloseSession(od.CartID);
            }

            string email = od.Email;
            var cokiesReader = CookiesHelper.Instance;
            var сookies = Request.Cookies;
            foreach (var item in сookies)
            {

                var keyCookie = item.Key;
                var valueCookie = item.Value;

                if (keyCookie == email)
                {
                    cokiesReader.CookiesDict.Remove(keyCookie);

                    var response = Request.HttpContext.Response;
                    response.Cookies.Delete(email);
                }
            }

            return mySuccess;
        }

        private void CloseSession(int cartId)
        {
            repository.CloseCartSession(cartId);
        }

        private string RegexPhone(string phone)
        {
            try
            {
                MatchCollection matches = Regex.Matches(phone, @"\d+");
                string result = "";
                foreach (System.Text.RegularExpressions.Match match in matches)
                {
                    result += match.ToString();
                }
                return result.Length > 10 ? result.Substring(0, 10) : result;
            }
            catch
            {
                return phone;
            }

        }

        private void DeletePasserCookies(QueryStringValues linkParametrs)
        {
            var response = Request.HttpContext.Response;
            response.Cookies.Delete("linkPasser");
            CloseRedirectToPasser();

        }
        private List<CartItem> GetSheetQuantities(IList<SheetSelectionExistModel> data, int cartId)
        {
            string customerGuid = data.FirstOrDefault().CustomerGuid;
            var subtotal = data.FirstOrDefault().Subtotal;
            string catalogDiscount = data.FirstOrDefault().CatalogDiscount;
            var catalogDiscountPerc = data.FirstOrDefault().CatalogDiscountPerc;

            List<CartItem> cartItemList = new List<CartItem>();
            if (data != null)
            {
                foreach (var item in data)
                {
                    if (item.ProductID > 0)
                    {
                        CartItem ci = new CartItem();
                        ci.CartID = cartId;
                        ci.ImageID = item.ImageID;
                        ci.ProductID = item.ProductID;
                        ci.Quantity = item.Quantity;



                        ci.Price = ci.ProductID == 4 ? Convert.ToDecimal(item.MagnetsPrice) : Convert.ToDecimal(item.SheetsPrice);
                        ci.PriceWithReward = ci.ProductID == 4 ? Convert.ToDecimal(item.MagnetsPrice) : Convert.ToDecimal(item.SheetsPrice);
                        ci.PriceWithoutReward = ci.ProductID == 4 ? Convert.ToDecimal(item.MagnetsPrice) : Convert.ToDecimal(item.SheetsPrice);

                        cartItemList.Add(ci);
                    }
                    if (item.ProductType == "Collage")
                    {
                        int rew = 0;

                        if (!string.IsNullOrEmpty(item.Reward))
                        {
                            rew = Convert.ToInt32(item.Reward);
                        }
                        int NewQty = item.Quantity - rew;
                        if (rew > 0)
                        {
                            CartItem cItem = new CartItem();
                            cItem.CartID = cartId;
                            cItem.ImageID = -1;
                            cItem.ProductID = 5;
                            cItem.Quantity = rew;
                            cItem.Price = 0.00M;

                            cartItemList.Add(cItem);
                        }
                        if (NewQty > 0)
                        {
                            CartItem ci = new CartItem();
                            ci.CartID = cartId;
                            ci.ImageID = -1;
                            ci.ProductID = 5;
                            ci.Quantity = item.Quantity;
                            ci.Price = item.CollagePrice;

                            cartItemList.Add(ci);
                        }

                    }
                    if (item.ProductType == "Keepsake")
                    {
                        int rew = 0;

                        if (!string.IsNullOrEmpty(item.Reward))
                        {
                            rew = Convert.ToInt32(item.Reward);
                        }
                        int NewQty = item.Quantity - rew;
                        if (rew > 0)
                        {
                            CartItem cItem = new CartItem();
                            cItem.CartID = cartId;
                            cItem.ImageID = -1;
                            cItem.ProductID = 6;
                            cItem.Quantity = rew;
                            cItem.Price = 0.00M;

                            cartItemList.Add(cItem);
                        }
                        if (NewQty > 0)
                        {
                            CartItem ci = new CartItem();
                            ci.CartID = cartId;
                            ci.ImageID = -1;
                            ci.ProductID = 6;
                            ci.Quantity = item.Quantity;
                            ci.Price = item.KeepsakePrice;

                            cartItemList.Add(ci);
                        }
                    }
                    if (item.ProductType == "Digital")
                    {
                        int rew = 0;

                        if (!string.IsNullOrEmpty(item.Reward))
                        {
                            rew = Convert.ToInt32(item.Reward);
                        }
                        int NewQty = item.Quantity - rew;
                        if (rew > 0)
                        {
                            CartItem cItem = new CartItem();
                            cItem.CartID = cartId;
                            cItem.ImageID = -1;
                            cItem.ProductID = 7;
                            cItem.Quantity = rew;
                            cItem.Price = 0.00M;

                            cartItemList.Add(cItem);
                        }
                        if (NewQty > 0)
                        {
                            CartItem ci = new CartItem();
                            ci.CartID = cartId;
                            ci.ImageID = -1;
                            ci.ProductID = 7;
                            ci.Quantity = item.Quantity;
                            ci.Price = item.DigitalPrice;

                            cartItemList.Add(ci);
                        }
                    }
                }
            }

            return cartItemList;
        }
        private void SaveCookie(string customerGuid, int cartId)
        {
            string txtEmail = repository.GetEmailByCustomerGuid(customerGuid);
            var response = Request.HttpContext.Response;

            Microsoft.AspNetCore.Http.CookieOptions cookOption = new Microsoft.AspNetCore.Http.CookieOptions();
            cookOption.Expires = DateTime.Now.AddDays(1);
            var str = "customerGuid&" + customerGuid + "&cartId&" + cartId;

            var cryptStr = Crypt.EncryptStringToBytes_Aes(str);

            response.Cookies.Append(txtEmail, cryptStr, cookOption);

            var cokiesReader = CookiesHelper.Instance;
            var сookies = Request.Cookies;

            bool isInclude = true;
            foreach (var item in cokiesReader.CookiesDict)
            {
                if (item.Key == txtEmail)
                {
                    isInclude = false;
                }
            }
            if (isInclude)
            {
                cokiesReader.CookiesDict.Add(txtEmail, str);
            }


            var updLink = new QueryStringValues();
            updLink.CartID = cartId.ToString();
            UpdateLinksParametrs(updLink);
        }

        private void TestIsVisited(QueryStringValues linkParametrs)
        {
            if (linkParametrs.IsAdmin && !linkParametrs.IsVisited)
            {
                linkParametrs.IsVisited = true;
                var response = Request.HttpContext.Response;

                var modelJson = JsonConvert.SerializeObject(linkParametrs);
                var encryptModelJson = Helpers.Crypt.EncryptStringToBytes_Aes(modelJson);

                Microsoft.AspNetCore.Http.CookieOptions cookOption = new Microsoft.AspNetCore.Http.CookieOptions();
                cookOption.Expires = DateTime.Now.AddDays(1);
                response.Cookies.Append("linkPasser", encryptModelJson, cookOption);
            }
        }

        private void CloseRedirectToPasser()
        {
            string link = "";
            var сookies = Request.Cookies;
            foreach (var item in сookies)
            {
                if (item.Key == "link")
                {
                    link = item.Value;
                }
            }

            var edecriptModel = Helpers.Crypt.DecryptStringFromBytes_Aes(link);
            var model = JsonConvert.DeserializeObject<QueryStringValues>(edecriptModel);

            if (model != null)
            {
                model.RedirectToPasser = false;
            }
            var response = Request.HttpContext.Response;

            var modelJson = JsonConvert.SerializeObject(model);
            var encryptModelJson = Helpers.Crypt.EncryptStringToBytes_Aes(modelJson);

            Microsoft.AspNetCore.Http.CookieOptions cookOption = new Microsoft.AspNetCore.Http.CookieOptions();
            cookOption.Expires = DateTime.Now.AddDays(1);
            response.Cookies.Append("link", encryptModelJson, cookOption);
        }
        public bool UpdateLinksParametrs(QueryStringValues model)
        {
            bool result = true;

            var linkParametrs = ReadCookies();

            //new parametrs
            string customerGuid = model.CustomerGUID;
            string bookingGuid = model.BookingSittingGUID;
            int cartid = Convert.ToInt32(model.CartID);
            int bookingID = Convert.ToInt32(model.BookingID);

            if (!string.IsNullOrEmpty(customerGuid))
            {
                linkParametrs.CustomerGUID = customerGuid;
            }
            if (!string.IsNullOrEmpty(bookingGuid))
            {
                linkParametrs.BookingSittingGUID = bookingGuid;
            }
            if (!string.IsNullOrEmpty(cartid.ToString()) && cartid > 0)
            {
                linkParametrs.CartID = cartid.ToString();
            }
            if (!string.IsNullOrEmpty(bookingID.ToString()) && bookingID > 0)
            {
                linkParametrs.BookingID = bookingID.ToString();
            }

            var response = Request.HttpContext.Response;

            var modelJson = JsonConvert.SerializeObject(linkParametrs);
            var encryptModelJson = Helpers.Crypt.EncryptStringToBytes_Aes(modelJson);

            try
            {
                Microsoft.AspNetCore.Http.CookieOptions cookOption = new Microsoft.AspNetCore.Http.CookieOptions();
                cookOption.Expires = DateTime.Now.AddDays(1);
                response.Cookies.Append("link", encryptModelJson, cookOption);
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }
    }
}
