﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Text;
using System.Net.Http;
using System.Linq;
using System.Security.Cryptography;
using System.Globalization;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Net.Http.Headers;

using VirtualPass.CE4.Core;
using VirtualPass.CE4.Models;
using VirtualPass.CE4.Models.AccountViewModels;
using VirtualPass.CE4.Services;
using NewCoreDll;
using VirtualPass.CE4.Models.ManageViewModels;
using VirtualPass.CE4.Helpers;
using Newtonsoft.Json;
using VirtualPass.Token;
using System.IO;
using System.Xml.XPath;

namespace VirtualPass.CE4.Controllers
{
    [Authorize]
    public class PasserAdminController : Controller
    {
        IConfigurationRoot appConfiguration;
        readonly ILogger logger;
        readonly UserManager<ApplicationUser> userManager;
        readonly SignInManager<ApplicationUser> signInManager;
        private readonly IHostingEnvironment hostingEnvironment;
        readonly IRepository repository;
        readonly IImageProvider imagesProvider;
        private DigitalCEContext db = null;
        private ILoggerFactory loggerFactory;
        private IServiceProvider provider;

        public static string cropped = ".cropped.jpg";
        private static string zerosittinguid = "AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAAAAAA";

        public PasserAdminController(
                IConfigurationRoot appConfiguration,
                UserManager<ApplicationUser> userManager,
                SignInManager<ApplicationUser> signInManager,
                ILoggerFactory _loggerFactory,
                IRepository repository,
                IImageProvider imagesProvider,
                IHostingEnvironment hostingEnvironment,
                IServiceProvider provider
                )
        {
            this.appConfiguration = appConfiguration;
            this.userManager = userManager;
            loggerFactory = _loggerFactory;
            logger = loggerFactory.CreateLogger<PasserAdminController>();
            this.signInManager = signInManager;
            this.repository = repository;
            this.imagesProvider = imagesProvider;
            this.hostingEnvironment = hostingEnvironment;
            this.provider = provider;
            db = repository.Context();
        }

        #region UserIdentity
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    logger.LogInformation(1, "User logged in.");
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return View(model);
                }
            }
            return View(model);
        }

        public async Task<IActionResult> Index()
        {
            int employeeID = 0;
            if (this.User.Identity.IsAuthenticated)
            {
                var userEmployeeID = this.User.FindFirstValue("EmployeeID");
                try { employeeID = Convert.ToInt32(userEmployeeID); } catch { }
            }

            CoverBookingModel cbooking = new CoverBookingModel()
            {
                TableOfBookings = await repository.GetBookingInfo(employeeID),
                CandarOfBookings = await repository.GetBookingInfoAroundDate(employeeID, 14),
                EmployeeID = employeeID
            };
            return View(cbooking);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await signInManager.SignInAsync(user, isPersistent: false);
                    logger.LogInformation(3, "User created a new account with password.");
                    var token = TokenProviderOptions.GenerateToken(user);
                    ShowToken(token);
                    await userManager.SetAuthenticationTokenAsync(user, "Bearer", "Token", token);

                    return RedirectToAction("Index");
                }
                AddErrors(result);
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult ShowToken(string model)
        {
            return View(model);
        }

        void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        public async Task<IActionResult> LogOut()
        {
            await signInManager.SignOutAsync();
            logger.LogInformation(4, "User logged out.");
            return RedirectToAction("Index");
        }

        #endregion UserIdentity

        #region Pass Preparation
        delegate string ProcessTask(string id);
        MyLongRunningClass longRunningClass = new MyLongRunningClass();

        /// <summary>
        /// Starts the long running process.
        /// </summary>
        /// <param name="id">The id.</param>
        public void StartLongRunningProcess(string id)
        {
            if (string.IsNullOrEmpty(id)) return;
            if (longRunningClass.Add(id))
            {
                ProcessTask processTask = new ProcessTask(longRunningClass.ProcessLongRunningAction);
                //processTask.BeginInvoke(id, new AsyncCallback(EndLongRunningProcess), processTask);
                //processTask.BeginInvoke(id, new AsyncCallback(EndLongRunningProcess),null);
                Thread mythread = new Thread(() => processTask.Invoke(id));
                mythread.Start();
            }
        }

        public void StopLongRunningProcess(string pid)
        {
            if (!string.IsNullOrEmpty(pid))
                longRunningClass.Remove(pid);
        }

        /// <summary>
        /// Ends the long running process.
        /// </summary>
        /// <param name="result">The result.</param>
        public void EndLongRunningProcess(IAsyncResult result)
        {
            ProcessTask processTask = (ProcessTask)result.AsyncState;
            string id = processTask.EndInvoke(result);
            longRunningClass.Remove(id);
        }

        /// <summary>
        /// Gets the current progress.
        /// </summary>
        /// <param name="id">The id.</param>
        public ContentResult GetCurrentProgress(string id)
        {
            //this.ControllerContext.HttpContext.Response.AddHeader("cache-control", "no-cache");
            var currentProgress = longRunningClass.GetStatus(id).ToString();
            return Content(currentProgress);
        }

        public async Task<IActionResult> PassPreparation(int bookingID)
        {
            BookingModel model = (from b in db.Booking
                                  join i in db.BookingInfo on b.BookingID equals i.BookingID
                                  join p in db.PricingStructure on b.PricingStructureID equals p.PricingStructureID
                                  where b.BookingID == bookingID
                                  select new BookingModel()
                                  {
                                      BookingID = i.BookingID,
                                      Tax = ((decimal)(i.TaxRate.HasValue ? i.TaxRate : 0)).ToString("P"),
                                      PricingStructureName = p.PricingStructureDescription,
                                      BookingNotes = i.BookingNotes,
                                      ShootDate = i.ShootDate,
                                      Status = i.Status

                                  }).FirstOrDefault();

            //if (status != null) model.Status = (BookingStatus)status;
            //var fff =appConfiguration["PasserAdminBridgeUrl"];
            @ViewData["LocalBridgeUrl"] = appConfiguration["PasserAdminBridgeUrl"];
            Claim clold = User.Claims.Where(l => l.Type == "EmployeeID").FirstOrDefault();
            @ViewData["employeeid"] = clold != null ? clold.Value : "0";

            var d = new BookingImporter(provider);
            d.Process();

            return View(model);
        }
        #endregion Pass Preparation

        #region Child Select

        public async Task<IActionResult> ChildSelection(string bookingId)
        {
            int intBookingId;
            if (string.IsNullOrEmpty(bookingId) || !int.TryParse(bookingId, out intBookingId))
            {
                return RedirectToAction("Error");
            }
            ChildSelectFormer former = new ChildSelectFormer();
            IList<ChildViewModel> model = await former.GetModel(intBookingId, repository);
            ViewData["BookingID"] = bookingId;
            return View(model);
        }


        public int RemoveSitting(string BookingSittingGUID, int BookingSittingID)
        {
            if (string.IsNullOrEmpty(BookingSittingGUID)) return 0;
            db = repository.Context();
            var d = db.Sittings.SingleOrDefault(x => x.BookingSittingGUID == BookingSittingGUID);
            if (d != null)
            {
                try
                {
                    db.Sittings.Remove(d);
                    db.SaveChanges();
                }
                catch
                {
                    return 0;
                }
            }
            else return 0;

            return BookingSittingID;
        }
        #endregion Child Select

        #region Prepare for Pass

        public JsonResult Booking_InsertNew(string sesionID, int priceStructure)
        {
            db = repository.Context();
            bool exist = db.Booking.Any(b => b.SessionID.Equals(sesionID));

            if (!exist)
            {
                db.Booking.Add(new Booking
                {
                    BookingID = Convert.ToInt32(sesionID),
                    RunsheetFinalized = null,
                    SessionID = sesionID,
                    PricingStructureID = priceStructure
                });
            }

            return Json(db.Booking.Where(b => b.SessionID.Equals(sesionID))
                .Select(b => b.BookingID)
                .FirstOrDefault());
        }

        public async Task<IActionResult> PrepareforPass(string BookingSittingGUID, int bookingID, int unattached = 0)
        {
            db = repository.Context();
            string ip = Response.HttpContext.Connection.RemoteIpAddress.ToString();
            int employeeID = 0;
            if (this.User.Identity.IsAuthenticated)
            {
                Claim clold = User.Claims.Where(l => l.Type == "EmployeeID").FirstOrDefault();
                try { employeeID = Convert.ToInt32(clold == null ? "0" : clold.Value); } catch { }
            }
            ViewData["EmployeeID"] = employeeID;

            var model = new PrepareforPassModel(appConfiguration["PasserAdminBridgeUrl"]);

            string currentbookigsittingguid = string.Empty;
            int bookingSittingID = 0;
            if (BookingSittingGUID == "0")
            {
                var roster = new Roster { BookingID = bookingID };
                db.Rosters.Add(roster);
                var rosterChild = new RosterChildren
                { RosterID = roster.RosterID };
                db.RosterChildren.Add(rosterChild);
                db.SaveChanges();
                string accessCode = string.Empty;
                int itried = 0;
                bool duplicate = true;
                while (duplicate && itried < 10)
                {
                    accessCode = appConfiguration["AccesCodePrefix"] + Guid.NewGuid().ToString("N").Substring(0, 8).ToUpper();
                    itried++;
                    duplicate = (db.Sittings.Where(h => h.AccessCode == accessCode).Count() > 0);
                }
                int? maxSitting = db.Sittings.Where(k => k.BookingID == bookingID).OrderByDescending(i => i.SittingID).Select(n => n.SittingID).FirstOrDefault();
                var sit = new BookingSitting
                {
                    RosterID = roster.RosterID,
                    BookingID = bookingID,
                    SittingStatusID = 1,
                    SittingID = (int)(maxSitting == null ? 1 : maxSitting + 1),
                    BookingSittingGUID = Guid.NewGuid().ToString(),
                    AccessCode = accessCode
                };
                db.Sittings.Add(sit);
                db.SaveChanges();
                BookingSittingGUID = sit.BookingSittingGUID;
                ViewData["BookingSittingID"] = sit.BookingSittingID;
                bookingSittingID = sit.BookingSittingID;
                ViewData["RosterID"] = sit.RosterID;
                ViewData["BookingSittingGUID"] = sit.BookingSittingGUID;
            }
            else
            {
                var sit = db.Sittings.Where(s => s.BookingSittingGUID == BookingSittingGUID).FirstOrDefault();
                ViewData["BookingSittingID"] = sit.BookingSittingID;
                bookingSittingID = sit.BookingSittingID;
                ViewData["AccessCode"] = sit.AccessCode;
                ViewData["RosterID"] = sit.RosterID;
                ViewData["BookingSittingGUID"] = sit.BookingSittingGUID;
            }

            ViewData["CountImagesAttached"] = db.Images.Where(i => i.BookingID == bookingID && i.BookingSittingID == bookingSittingID && i.ShooterID == employeeID && i.ImageStatusID != (int)ImageStatusK.ShoulbeDeletedInHotFolder).Count();
            ViewData["CountImagesUnAttached"] = db.Images.Where(i => i.BookingID == 0 && i.ShooterID == employeeID && i.ImageStatusID != (int)ImageStatusK.ShoulbeDeletedInHotFolder).Count();


            var teachers = (from b in db.RosterChildren
                            where b.TeacherName != null && b.TeacherName != "" && b.TeacherName.ToLower() != "null"
                            orderby b.TeacherName
                            select new SelectListItem { Text = b.TeacherName, Value = b.TeacherName }).Distinct();

            ViewData["Teachers"] = teachers;
            model.sitting = (await repository.GetSittings(new List<String>() { BookingSittingGUID })).FirstOrDefault();
            model.children = (await repository.GetChildren((int)model.sitting.RosterID));
            if (model.children.Director == null) model.children.Director = false;
            if (model.children.Staff == null) model.children.Staff = false;
            ViewData["BookingID"] = bookingID;

            var inFamilySitting = (from fam in db.Sittings
                                   join lnk in db.SittingLinks on fam.BookingSittingID equals lnk.BookingSittingID
                                   where fam.BookingSittingID == model.sitting.BookingSittingID
                                   select lnk.ParentBookingSittingID).ToList();
            ViewData["InFamilySitting"] = inFamilySitting;

            var allFamilySitting = (from fam in db.Sittings
                                    join rst in db.RosterChildren on fam.RosterID equals rst.RosterID
                                    where fam.BookingID == bookingID && fam.BookingSittingGUID != currentbookigsittingguid
                                                                     && ((rst.ChildLastName + rst.ChildFirstName).Trim() != string.Empty)
                                    select new SelectListItem { Text = string.Join(" ", new string[] { rst.ChildFirstName, rst.ChildLastName }).ToUpper(), Value = fam.BookingSittingID.ToString() }).Distinct().ToList();
            SelectListItem item = null;
            if (inFamilySitting.Count > 0) item = (allFamilySitting.Find(n => n.Value == inFamilySitting[0].ToString()));
            bool selectdefault = false;
            if (item != null) item.Selected = true;
            else selectdefault = true;
            allFamilySitting.Add(new SelectListItem { Text = "NOT SET", Value = "0", Selected = selectdefault });
            ViewData["AllFamilySitting"] = allFamilySitting;

            var familyGroup = (from fam in db.Sittings
                               join rst in db.SittingLinks on fam.BookingSittingID equals rst.BookingSittingID
                               join set in db.RosterChildren on fam.RosterID equals set.RosterID
                               where inFamilySitting.Count != 0 && rst.ParentBookingSittingID == inFamilySitting[0]
                               select new SelectListItem { Text = string.Join(" ", new string[] { set.ChildFirstName, set.ChildLastName }).ToUpper(), Value = fam.BookingSittingGUID.ToString() }).Distinct().ToList();

            ViewData["FamilyGroup"] = familyGroup;

            return View(model);





        }

        [HttpGet]
        public ActionResult GetImageInfo(string imageID)
        {
            Image model = null;
            try
            {
                db = repository.Context();
                var lookupId = int.Parse(imageID);
                model = db.Images.Where(i => i.ImageID == lookupId).FirstOrDefault();
            }
            catch { }
            return PartialView("_ImageInfo", model);
        }

        [HttpGet]
        public ActionResult GetCrop(int imageID)
        {
            PasserAdminFormer paf = new PasserAdminFormer();
            var img = paf.GetImage(imageID, repository);
            if (img == null) return new EmptyResult();
            var anw = Json(new
            {
                autocropsourcex = img.AutoCropSourceX,
                autocropsourcey = img.AutoCropSourceY,
                autocropwidthpercentage = img.AutoCropWidthPercentage,
                autocropheightpercentage = img.AutoCropHeightPercentage,
                rotation = 0,
                imageid = img.ImageID
            });
            return anw;
        }

        [HttpPost]
        public int SaveImageInfo([FromBody] Image img)
        {
            if (img == null) return -1;
            db = repository.Context();
            if (img.ImageID == -1) return -3;
            else
            {
                var s = db.Images.Where(i => i.ImageID == img.ImageID).FirstOrDefault();
                if (s == null) return -2;

                s.Graduate = img.Graduate;
                s.CustomField1 = img.CustomField1;
                s.CustomField2 = img.CustomField2;
                db.SaveChanges();
            }
            return 1;
        }

        [HttpPost]
        public int SaveImage([FromBody] Image img)
        {
            if (img == null) return -1;
            db = repository.Context();
            int imageID = -1;
            if (img.ImageID != -1)
            {
                imageID = img.ImageID;
            }
            else
            {
                int? s = (from b in db.Booking
                          join i in db.Images on b.BookingID equals i.BookingID
                          where System.IO.Path.GetFileName(i.CroppedImgPath).ToLower().Trim() == System.IO.Path.GetFileName(img.CroppedImgPath).ToLower().Trim()
                          select i.ImageID).FirstOrDefault();
                if ((s != null) && (s != 0)) { }
                Image image = new Image
                {
                    FrameID = img.FrameID,
                    Pose = img.FrameID,
                    BookingID = img.BookingID,
                    ImageTypeID = (int)VirtualPass.CE4.Core.ImageType.Preview,
                    BookingSittingID = img.BookingSittingID,
                    ImageStatusID = (int)ImageStatusK.MillersIngestComplete,
                    DNGPath = "",
                    DP2Path = "",
                    RAWPath = img.RAWPath,
                    CroppedImgPath = img.CroppedImgPath,
                    PreviewPath = "",
                    Roll = "",
                    ThumbnailPath = "",
                    AutoCropSourceX = 50,
                    AutoCropSourceY = 50,
                    AutoCropWidthPercentage = 100,
                    AutoCropHeightPercentage = 100,
                    Graduate = img.Graduate,
                    CustomField1 = img.CustomField1,
                    CustomField2 = img.CustomField2

                };
                db.Images.Add(image);
                db.SaveChanges();
                imageID = image.ImageID;
                repository.SetBookingStatus(image.BookingID, BookingStatus.Ready);
            }
            return imageID;
        }

        [HttpPost]
        public int SavePose(int imageid, int pose)
        {
            db = repository.Context();
            var img = db.Images.Where(i => i.ImageID == imageid).FirstOrDefault();
            if (img != null)
            {
                img.FrameID = pose;
                img.Pose = pose;
                db.SaveChanges();
            }
            return 0;
        }

        [HttpPost]
        public ActionResult GetUnattached(int employeeid, int group, int page = 0, int pageSize = 500, string except = "")
        {
            var query = repository.GetBookingImages(0, employeeid);
            var results1 = query.Result
                       .Skip(pageSize * page)
                       .Take(pageSize)
                       .ToList();
            List<Image> result2 = new List<Image>();

            if ((except != null) && (except != string.Empty))
            {
                int[] exceptimages = except.Split(',').Select(x => Int32.Parse(x)).ToArray();
                results1.RemoveAll(p => exceptimages.Contains(p.ImageID));
            }

            bool first = true;
            bool second = false;
            DateTime? current = null;
            foreach (Image i in results1)
            {
                try
                {
                    if (current == null) current = i.EXIFDATE;
                    else
                    {
                        TimeSpan span = current.Value.Subtract(i.EXIFDATE.Value);
                        if (Math.Abs(span.Minutes) > 30)
                        {
                            first = false;
                            second = true;
                        }
                        current = i.EXIFDATE;
                    }
                }
                catch
                {
                }
                if ((group == 0) || (group == 1 && first) || (group == 2 && second))
                {
                    i.DP2Path = Url.Action("ImagesPreview", "BookingHelper", new { bookingID = i.BookingID, fileName = System.IO.Path.GetFileName(i.CroppedImgPath), media = 150 });
                    i.CroppedImgPath = Url.Action("ImagesPreview", "BookingHelper", new { bookingID = i.BookingID, fileName = System.IO.Path.GetFileName(i.CroppedImgPath), media = 1280 });
                    result2.Add(i);
                }
            }
            return Json(result2);
        }

        [HttpPost]
        public int AttachImage([FromBody] AttachedPhoto photo)
        {

            if (photo == null) return -1;
            db = repository.Context();
            var img = db.Images.Where(i => i.ImageID == photo.ImageID).FirstOrDefault();
            if (img == null) return -1;

            imagesProvider.MovePreview(img.BookingID, img.CroppedImgPath, photo.BookingID);
            if (photo.Pose == 0)
            {
                int? pose = db.Images.Where(i => i.BookingSittingID == photo.BookingSittingID).Max(i => i.Pose);
                photo.Pose = (int)(pose == null ? 1 : (pose + 1));
            }
            img.Pose = photo.Pose;
            img.ImageStatusID = (int)ImageStatusK.MillersIngestComplete;
            img.BookingSittingID = photo.BookingSittingID;
            img.FrameID = photo.Pose;
            img.BookingID = photo.BookingID;
            db.SaveChanges();
            repository.SetBookingStatus(img.BookingID, BookingStatus.Ready);
            return photo.Pose;
        }

        [HttpPost]
        [Authorize("Bearer")]
        public int SaveImageUnattached([FromBody] PhotoFile img)
        {
            if (img != null)
            {
                db = repository.Context();
                int? se = (from i in db.Images
                           join t in db.Sittings on i.BookingSittingID equals t.BookingSittingID
                           where (i.CroppedImgPath.ToLower().Trim() == (img.FullName + cropped).ToLower().Trim()
                           && img.ShooterID == i.ShooterID
                           && img.Hash.ToLower() == i.Hash.ToLower()
                           )
                           select i.ImageID).FirstOrDefault();
                if ((se != null) && (se != 0))
                {
                    if (imagesProvider.ExistPreview("0", img.FileName)) return -1;
                    return (int)se;
                }
                int? sh = (from i in db.Images
                           join t in db.Sittings on i.BookingSittingID equals t.BookingSittingID
                           where (i.CroppedImgPath.ToLower().Trim() == (img.FullName + cropped).ToLower().Trim()
                                   && img.ShooterID == i.ShooterID
                                 )
                           select i.ImageID).FirstOrDefault();
                if ((sh != null) && (sh != 0)) return (int)sh;

                int? zerrosittingid = (from m in db.Sittings
                                       where m.BookingSittingGUID == zerosittinguid
                                       select m.BookingSittingID).FirstOrDefault();
                if ((zerrosittingid == null) || (zerrosittingid == 0))
                {
                    if (db.Booking.Where(b => b.BookingID == 0).Count() == 0)
                    {
                        var booking = new Booking()
                        {
                            BookingID = 0,
                        };
                        db.Booking.Add(booking);
                        db.SaveChanges();
                    }
                    var sitt = new BookingSitting()
                    {
                        BookingID = 0,
                        SittingID = 0,
                        SittingStatusID = 1,
                        BookingSittingGUID = zerosittinguid
                    };
                    db.Sittings.Add(sitt);
                    db.SaveChanges();
                    zerrosittingid = sitt.BookingSittingID;
                }
                //
                Image image = new Image
                {
                    FrameID = 0,
                    Pose = 0,
                    BookingID = 0,
                    ImageTypeID = (int)VirtualPass.CE4.Core.ImageType.Preview,
                    BookingSittingID = (int)zerrosittingid,
                    ImageStatusID = (int)ImageStatusK.Downloading,
                    DNGPath = "",
                    DP2Path = "",
                    RAWPath = img.FileName,
                    CroppedImgPath = img.FullName + cropped,
                    PreviewPath = "",
                    Roll = "",
                    ThumbnailPath = "",
                    AutoCropSourceX = 50,
                    AutoCropSourceY = 50,
                    AutoCropWidthPercentage = 100,
                    AutoCropHeightPercentage = 100,
                    ComputerName = img.ComputerName,
                    Hash = img.Hash,
                    ShooterID = img.ShooterID,
                    FileSize = (int)img.FileSize,
                    Graduate = false,
                    LoadDate = DateTime.Now
                };
                db.Images.Add(image);
                db.SaveChanges();
                return image.ImageID;
            }
            return 0;
        }

        [HttpPost]
        public int RemoveImage([FromBody] Image img)
        {//
            PasserAdminFormer paf = new PasserAdminFormer();
            return paf.RemoveImage(img, repository, imagesProvider);
        }

        [Authorize("Bearer")]
        public int RemoveImageByID(int imageID)
        {//
            var img = new Image() { ImageID = imageID };
            PasserAdminFormer paf = new PasserAdminFormer();
            return paf.RemoveImage(img, repository, imagesProvider);
        }

        [Authorize("Bearer")]
        public ActionResult GetDeletedUnAttached(int ShooterID, string Computer)
        {
            PhotoFiles photofiles = new PhotoFiles();
            db = repository.Context();
            var img = db.Images.Where(i => i.ComputerName == Computer && i.ShooterID == ShooterID && i.ImageStatusID == (int)ImageStatusK.ShoulbeDeletedInHotFolder).Select
                (p => new PhotoFile()
                {
                    FileName = p.CroppedImgPath,
                    FullName = p.RAWPath,
                    Hash = p.Hash,
                    ImageID = p.ImageID,
                }).ToArray();
            photofiles.photos = img;
            return Json(photofiles);
        }

        [HttpPost]
        public int CountImagesbyBooking(int bookingID, int employeeid)
        {
            var ii = db.Images.Where(m => m.BookingID == bookingID && m.ShooterID == employeeid && m.ImageStatusID != (int)ImageStatusK.ShoulbeDeletedInHotFolder).Count();
            return ii;
        }

        [HttpPost]
        public int SaveCrop([FromBody] ImageCropInfo info)
        {
            PasserAdminFormer paf = new PasserAdminFormer();
            return paf.SaveCrop(info, repository); ;
        }

        [HttpPost]
        public int SaveRoster([FromBody] ChildInfo child)
        {
            PasserAdminFormer paf = new PasserAdminFormer();
            return paf.SaveChild(child, repository);
        }

        [HttpGet]
        [Authorize("Bearer")]
        public int DoesImageRecordExist(int imageID)
        {
            PasserAdminFormer paf = new PasserAdminFormer();
            int? c1 = paf.GetImageIDOfMinImage(repository);
            int? c2 = paf.GetImageID(imageID, repository);
            if ((c1 != null) && (c2 == null)) return 0;
            return 1;
        }

        public FileResult Download(string file)
        {
            var fileName = hostingEnvironment.WebRootPath + @"\download\" + file;
            if (!System.IO.File.Exists(fileName)) return null;
            byte[] fileBytes = System.IO.File.ReadAllBytes(fileName);
            return File(fileBytes, "application/x-msdownload", file);
        }

        #region use from wcf-bridge
        [HttpGet]
        public ActionResult GetBookingStatus(int bookingID)
        {
            db = repository.Context();
            var r = new FileNameParcesCollection();
            r.mem = db.Images.Where(i => i.BookingID == bookingID && i.ImageStatusID != ((int)(ImageStatusK.TransferComplete))).Select
                (dr => new FileNameParces("")
                {
                    FileName = dr.CroppedImgPath,
                    BookingID = dr.BookingID.ToString(),
                    ImageID = dr.ImageID
                }).ToArray();
            r.totalcount = db.Images.Where(i => i.BookingID == bookingID).Count();
            r.bookingid = bookingID;
            if (r.totalcount == 0) r.completeness = 0;
            else
            {
            }
            r.completeness = r.totalcount == 0 ? 0 : (int)Math.Floor((((double)(r.totalcount - r.mem.Length)) / (double)r.totalcount) * 100.00);
            return Json(r);
        }

        [HttpGet]
        public ActionResult SetStatusOriginalTransfering(int imageID)
        {
            PasserAdminFormer paf = new PasserAdminFormer();
            paf.SetImageStatus(imageID, ImageStatusK.TransferStart, repository);
            return Ok();
        }

        [HttpGet]
        [Authorize("Bearer")]
        public ActionResult GetUnTransferedYet(int bookingID, int ShooterID, string Computer)
        {
            PhotoFiles photofiles = new PhotoFiles();
            db = repository.Context();
            var img = db.Images
                    .Where(i => i.BookingID == bookingID && i.ShooterID == ShooterID && i.ImageStatusID == (int)ImageStatusK.MillersIngestComplete && i.ComputerName.ToLower() == Computer.ToLower())
                    .Select
                (p => new PhotoFile()
                {
                    FileName = p.CroppedImgPath,
                    FullName = p.RAWPath,
                    Hash = p.Hash,
                    ImageID = p.ImageID,
                }).ToArray();
            photofiles.photos = img;
            return Json(photofiles);
        }

        [HttpPost]
        [Authorize("Bearer")]
        public IActionResult SaveOrinal(IFormFile file, int imageID, int bookingID)
        {
            if (Request.Form != null)
            {
                var files = Request.Form.Files;
                if ((files != null) && (files.Count > 0))
                {
                    foreach (var source in files)
                    {
                        var filename = Microsoft.Net.Http.Headers.ContentDispositionHeaderValue
                        .Parse(source.ContentDisposition)
                        .FileName
                        .Trim('"');
                        MemoryStream st = new MemoryStream();
                        source.CopyTo(st);
                        st.Position = 0;
                        var fileName = new FileInfo(filename).Name;
                        imagesProvider.UploadOrinal(bookingID.ToString(), fileName, st);
                        try
                        {
                            int result = CreateAllPreview(bookingID, fileName, imageID);
                            if (result != 0)
                                return NoContent();
                            PasserAdminFormer paf = new PasserAdminFormer();
                            paf.SetImageStatus(imageID, ImageStatusK.TransferComplete, repository);
                            repository.SetBookingStatus(bookingID, BookingStatus.Ready);
                        }
                        catch (Exception ex)
                        {
                            return BadRequest(ex.Message);
                        }
                    }
                }
            }
            return Ok();
        }

        [HttpGet]
        [Authorize("Bearer")]
        public IActionResult SaveEXIFDate(int imageid, string exifdate)
        {
            db = repository.Context();
            db.Images.Where(i => i.ImageID == imageid).FirstOrDefault().ImageStatusID = (int)ImageStatusK.UnAttached;
            db.SaveChanges();

            if ((exifdate == null) || (string.IsNullOrEmpty(exifdate))) { }
            else
            {
                exifdate = exifdate.Replace("\0", "");
                CultureInfo provider = CultureInfo.InvariantCulture;
                try
                {
                    DateTime dateCreated = DateTime.ParseExact(exifdate, "yyyy:MM:d H:m:s", provider);
                    var img = db.Images.Where(i => i.ImageID == imageid).FirstOrDefault();
                    if (img != null)
                    {
                        img.EXIFDATE = dateCreated;
                        db.SaveChanges();
                    }
                }
                catch { }
            }
            return Ok();
        }

        [Authorize("Bearer")]
        public IActionResult SaveOrinalSelf(IFormFile file, string Computer, int ShooterID)
        {
            if (Request.Form != null)
            {
                var files = Request.Form.Files;
                if ((files != null) && (files.Count > 0))
                {
                    foreach (var source in files)
                    {
                        var filename = Microsoft.Net.Http.Headers.ContentDispositionHeaderValue
                        .Parse(source.ContentDisposition)
                        .FileName
                        .Trim('"');

                        var img = db.Images.Where(i => i.ComputerName == Computer && i.ShooterID == ShooterID && i.RAWPath == filename).Select
                                    (p => new PhotoFile()
                                    {
                                        FileName = p.CroppedImgPath,
                                        FullName = p.RAWPath,
                                        Hash = p.Hash,
                                        BookingID = p.BookingID,
                                        ImageID = p.ImageID,
                                    }).FirstOrDefault();
                        if (img == null)
                        {
                            PhotoFile p = new PhotoFile();
                            int imageID = SaveImageUnattached(p);
                            CreateAllPreview(0, filename, imageID);
                        }
                        else
                        {
                            MemoryStream st = new MemoryStream();
                            source.CopyTo(st);
                            st.Position = 0;
                            imagesProvider.UploadOrinal(img.BookingID.ToString(), img.FileName, st);
                            using (var md5 = MD5.Create())
                            {
                                using (var stream = System.IO.File.OpenRead(filename))
                                {
                                    var g = System.Text.Encoding.ASCII.GetString(md5.ComputeHash(stream));
                                }
                            }
                            CreateAllPreview(img.BookingID, img.FullName, img.ImageID);

                            PasserAdminFormer paf = new PasserAdminFormer();
                            paf.SetImageStatus(img.ImageID, ImageStatusK.TransferComplete, repository);
                            repository.SetBookingStatus(Convert.ToInt32(img.BookingID), BookingStatus.Ready);
                        }
                    }
                }
            }
            return Ok();
        }

        int CreateAllPreview(int bookingID, string filename, int imageID)
        {
            int result = 0;
            var img = db.Images.Where(i => i.ImageID == imageID).FirstOrDefault();
            string original = imagesProvider.GetOriginal(bookingID, filename);
            int[] croppSizes = appConfiguration["CroppsSizes"].Split(',').Select(s => Int32.Parse(s)).ToArray();
            if ((croppSizes != null) && (croppSizes.Length > 0))
            {
                ImageCropInfo crop = new ImageCropInfo();
                crop.autocropsourcex = (double)(img.AutoCropSourceX == null ? 50 : img.AutoCropSourceX);
                crop.autocropsourcey = (double)(img.AutoCropSourceY == null ? 50 : img.AutoCropSourceY);
                crop.autocropwidthpercentage = (double)(img.AutoCropWidthPercentage == null ? 100 : img.AutoCropWidthPercentage);
                crop.autocropheightpercentage = (double)(img.AutoCropHeightPercentage == null ? 100 : img.AutoCropHeightPercentage);
                crop.bookingid = bookingID;
                crop.filename = filename;
                crop.previewsizes = appConfiguration["CroppsSizes"];
                string url = appConfiguration["ImageWorkerAPI"];
                string res = CreateProductAsync(crop, url).Result;
            }

            async Task<string> CreateProductAsync(ImageCropInfo product, string url)
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.PostAsJsonAsync("Api/Values", product);
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsStringAsync();
            }

            return result;
        }

        [HttpPost]
        [Authorize("Bearer")]
        public async Task<IActionResult> SavePreviewImage(IFormFile formFile, string size = "150", string bookingID = "0")
        {
            int count = 0;
            if (Request.Form != null)
            {
                var files = Request.Form.Files;
                if ((files != null) && (files.Count > 0))
                {
                    foreach (var source in files)
                    {
                        var filename = Microsoft.Net.Http.Headers.ContentDispositionHeaderValue
                        .Parse(source.ContentDisposition)
                        .FileName
                        .Trim('"');

                        FileNameParces fp = new FileNameParces(filename);
                        MemoryStream st = new MemoryStream();
                        source.CopyTo(st);
                        st.Position = 0;
                        imagesProvider.UploadPreview(bookingID, fp.FileName, size, st);
                        count++;
                    }
                }
            }
            return Ok(new { count });
        }

        [HttpPost]
        public async Task<IActionResult> SaveCropImage(IFormFile formFile)
        {
            int count = 0;

            if (Request.Form != null)
            {
                var files = Request.Form.Files;
                if ((files != null) && (files.Count > 0))
                {
                    foreach (var source in files)
                    {
                        var filename = Microsoft.Net.Http.Headers.ContentDispositionHeaderValue
                        .Parse(source.ContentDisposition)
                        .FileName
                        .Trim('"');
                        FileNameParces fp = new FileNameParces(filename);
                        MemoryStream st = new MemoryStream();
                        await source.CopyToAsync(st);
                        st.Position = 0;
                        imagesProvider.UploadCrop(fp.BookingID, fp.FileName, st);
                        count++;
                    }
                }
            }
            return Ok(new { count });
        }
        #endregion use from wcf-bridge

        #endregion  Prepare for Pass

        #region  check API
        [HttpGet]
        [AllowAnonymous]
        public IActionResult CheckAPI()
        {
            return View();
        }
        #endregion check API

        public IActionResult PasserDetails(string bookingID)
        {
            GetPDModel model = repository.GetPricingStructure(bookingID);
            model.BookingID = bookingID;
            model.ActivePriceStructureList = repository.GetPriceStructuresActiveList().Result.ToList();
            return View(model);
        }

        [HttpPost]
        public IActionResult GetPasserDetailsItems(string bookingID)
        {
            IEnumerable<PasserDetailsModel> model = repository.GetPasserDetails(bookingID);
            return Json(model);
        }


        [HttpPost]
        public string UpdateTaxRate(string data)
        {
            decimal myTaxRateOut = 0.00M;
            string bookingID = "";
            string newTaxRate = "";
            string oldTaxRate = "";
            decimal myTaxRate = -1;
            string result = "";
            try
            {
                if (!string.IsNullOrEmpty(data))
                {
                    var mas = data.Split('#');
                    bookingID = mas[0];
                    newTaxRate = mas[1];
                    oldTaxRate = mas[2];
                }

                oldTaxRate = oldTaxRate.Replace("%", "");
                oldTaxRate = oldTaxRate.Replace(" ", "");
                oldTaxRate = oldTaxRate.Replace(".", ",");


                newTaxRate = newTaxRate.Replace("%", "");
                newTaxRate = newTaxRate.Replace(" ", "");
                newTaxRate = newTaxRate.Replace(".", ",");

                newTaxRate = !string.IsNullOrWhiteSpace(newTaxRate) ? newTaxRate : "0";

                if (decimal.TryParse(newTaxRate, out myTaxRate))
                {
                    myTaxRate /= 100;

                    myTaxRateOut = repository.UpdateTaxRate(bookingID, myTaxRate);

                }
                result = myTaxRateOut.ToString().Replace(',', '.');

                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        [HttpPost]
        public string ChangePrice(string data)
        {
            string result = "false";
            string inputedPass = data;
            if (!string.IsNullOrEmpty(inputedPass) && (inputedPass.Trim().ToUpper() == password.ToUpper()))
            {
                result = "true";
            }

            return result;
        }

        [HttpPost]
        public string SelectPrice(string data)
        {
            string bookingID = "";
            string result = "false";
            var activePriceStructureList = repository.GetPriceStructuresActiveList().Result.ToList();
            string priceID = "";
            string priceStructure = "";

            if (!string.IsNullOrEmpty(data))
            {
                var mas = data.Split('#');

                priceStructure = mas[0];
                bookingID = mas[1];
            }

            foreach (var item in activePriceStructureList)
            {
                if (item.PricingStructure == priceStructure)
                {
                    priceID = item.PricingStructureID.ToString();
                }
            }

            int pricingID = 1;
            try
            {
                pricingID = Convert.ToInt32(priceID);
            }
            catch (Exception ex)
            {
                return result;
            }

            try
            {
                int bookID = Convert.ToInt32(bookingID);
                var res = repository.UpdatePricingStructureID(bookID, pricingID);
                result = "true";
            }
            catch (Exception ex)
            {
                logger.LogError("", ex);
                return result;
            }

            return result;
        }

        public IActionResult PasserAdminReport(string bookingID)
        {
            return View(int.Parse(bookingID));
        }

        [HttpPost]
        public IActionResult GetPasserAdminReportItems(int bookingID)
        {
            var report = repository.GetPasserAdminReport();
            var res = Json(report);
            return res;
        }

        public async Task<IActionResult> PasserCheckout(string cvalue, string value, int cartId, string ip, string oid, string bookingid)
        {

            if (string.IsNullOrEmpty(value) || string.IsNullOrEmpty(cvalue) || cartId < 1)
            {
                return RedirectToAction("Error");
            }
            QueryStringValues qsv = new QueryStringValues();
            qsv.CartID = cartId.ToString();
            qsv.CustomerGUID = cvalue;
            qsv.BookingSittingGUID = value;
            qsv.OrderID = oid;
            qsv.BookingID = bookingid;
            qsv.PageType = ip;

            PasserAdminFormer former = new PasserAdminFormer();
            PasserCheckoutModel model = await former.GetPasserCheckout(qsv, repository);

            if (model == null)
            {
                return RedirectToAction("Error");
            }

            return View(model);
        }

        [HttpPost]
        public string ProcessPasserOffline([FromBody] OrderDatails od)
        {
            string mySuccess = "false";
            if (od == null)
            {
                return mySuccess;
            }

            try
            {
                od.DiscountTypeID = -1;
                List<States> states = repository.GetStates_DDL().Result.ToList();
                List<CardTypes> cardTipes = repository.GetCardType_DDL().Result.ToList();
                if (!string.IsNullOrEmpty(od.State))
                {
                    od.StateID = Convert.ToInt32(states.Where(i => i.State == od.State).FirstOrDefault().StateID);
                }
                if (od.ShippingState == " Please select state")
                {
                    od.ShippingState = "";
                }
                if (!string.IsNullOrEmpty(od.ShippingState))
                {
                    od.ShippingStateID = Convert.ToInt32(states.Where(i => i.State == od.ShippingState).FirstOrDefault().StateID);
                }
                if (!string.IsNullOrEmpty(od.DiscountType))
                {
                    List<DiscountTypePr> discTypes = repository.GetDiscountTypeActiveList().Result.ToList();

                    foreach (var item in discTypes)
                    {
                        if (item.DiscountType == od.DiscountType)
                        {
                            od.DiscountTypeID = item.DiscountTypeID;
                        }
                    }
                }

                string myOrderStatus = od.OrderStatus;

                string myPayType = "";
                string LastDigits = "-1";

                string AuthNetTransID = "-1";
                int myOrderID = -1;

                od.Authorized = false;
                od.CheckoutType = "Passer";
                od.Phone = RegexPhone(od.Phone);
                if (od.CCCreditCardType == "Select Card Type")
                {
                    od.CCCreditCardTypeID = -1;
                }
                else
                {
                    od.CCCreditCardTypeID = Convert.ToInt32(cardTipes.Where(ci => ci.CardType == od.CCCreditCardType).FirstOrDefault().CardTypeID);
                }

                if (od.isCash == "yes")
                {
                    myPayType = "Cash";
                    //#ao if cash or check set all cc & check fields to -1
                    od.CheckNumber = "-1";
                    od.CCNumber = "-1";
                    od.CCSecurityCode = "-1";
                    od.CCNameOnCard = "-1";
                    od.CCExpirationYear = "-1";
                    od.CCExpirationMonth = "-1";
                    od.Authorized = true; //#ao updating Authorize only on case or check
                }
                else if (od.isCheck == "yes")
                {
                    myPayType = "Check";
                    //#ao if cash or check set all cc & cash fields to -1
                    od.CCNumber = "-1";
                    od.CCSecurityCode = "-1"; ;
                    od.CCNameOnCard = "-1";
                    od.CCExpirationYear = "-1";
                    od.CCExpirationMonth = "-1";
                    od.Authorized = true; //#ao updating Authorize only on case or check
                }
                else if (od.isCredit == "yes")
                {
                    od.CheckNumber = "-1";
                    myPayType = "Credit";
                    string CardNumber = od.CCNumber;
                    int beg = CardNumber.Length - 4;
                    LastDigits = CardNumber.Substring(beg, 4);
                }
                if (od.isCredit == "yes")
                {
                    try
                    {
                        od.CheckNumber = "-1";

                        OnlinePaymantSystem paymSyst = new OnlinePaymantSystem();
                        OnlinePaymantSystem.Step1Result status = paymSyst.SendUpPayments(od, repository);
                        if (status == OnlinePaymantSystem.Step1Result.Complete)
                        {
                            mySuccess = "true";
                        }
                        else
                        {
                            mySuccess = "false";
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex," Error in billing");
                        mySuccess = "false";
                    }
                }
                else
                {
                    od.OrderStatus = "Order Placed";
                    if (myOrderStatus == "Awaiting Payment")
                    {
                        myOrderID = od.OrderID;
                        if (myOrderID < 0)
                        {
                            myOrderID = repository.GetOrderIDByCartID(od.CartID);
                        }
                        //#ao and updating customer incase data changed by Admin
                        repository.UpdateCustomer(Guid.Parse(od.CustomerGUID), od.FirstName, od.LastName, od.Address1, od.Address2, od.City, od.StateID, od.Zip, od.Phone);

                        //#ao update discount on exiting order
                        if (od.DiscountAmount > 0)
                        {
                            repository.UpdateOrderDiscount(myOrderID, od.DiscountAmount, od.DiscountTypeID, od.Total, od.Tax);
                        }
                        //#ao update order status
                        repository.UpdateOrderStatus(myOrderID, od.OrderStatus);
                    }
                    else
                    {
                        repository.DeleteOrderIfExist(od.CustomerGUID);
                        myOrderID = repository.InsertHoldOrder(od);

                    }
                    OrderPaymentDetails opd = new OrderPaymentDetails();
                    opd.OrderID = myOrderID;
                    opd.PayType = myPayType;
                    opd.Amount = Convert.ToDecimal(od.Total);
                    opd.Authorized = od.Authorized;
                    opd.AuthNetTransID = AuthNetTransID;

                    opd.CCNumber = Crypt.EncryptStringToBytes_Aes(od.CCNumber);
                    opd.CCSecurity = od.CCSecurityCode;
                    opd.CCNameOnCard = od.CCNameOnCard;
                    opd.CCExpYear = od.CCExpirationYear;
                    opd.CCExpMonth = od.CCExpirationMonth;

                    opd.LastDigits = LastDigits;
                    opd.CardTypeID = od.CCCreditCardTypeID;
                    opd.CheckNumber = od.CheckNumber;
                    try
                    {
                        repository.InsertOrderPaymentOffline(opd, od.CartID);
                        repository.UpdateOrderAuthorized(opd.OrderID, opd.Authorized);
                        mySuccess = "true";
                    }
                    catch (IOException e)
                    {
                        Logger.Error(e, );
                    }
                }

            }
            catch (Exception ex)
            {

                return mySuccess;
            }
            return mySuccess;
        }

        public static string RegexPhone(string phone)
        {
            try
            {
                MatchCollection matches = Regex.Matches(phone, @"\d+");
                string result = "";
                foreach (System.Text.RegularExpressions.Match match in matches)
                {
                    result += match.ToString();
                }
                return result.Length > 10 ? result.Substring(0, 10) : result;
            }
            catch
            {
                return phone;
            }
        }

        protected bool SaveLinksParametrs(QueryStringValues model)
        {
            bool result = true;
            var response = Request.HttpContext.Response;

            var modelJson = JsonConvert.SerializeObject(model);
            var encryptModelJson = Helpers.Crypt.EncryptStringToBytes_Aes(modelJson);

            try
            {
                Microsoft.AspNetCore.Http.CookieOptions cookOption = new Microsoft.AspNetCore.Http.CookieOptions();
                cookOption.Expires = DateTime.Now.AddDays(1);
                response.Cookies.Append("linkPasser", encryptModelJson, cookOption);

            }
            catch (Exception ex)
            {
                result = false;
            }

            return result;
        }

        public async Task<IActionResult> PasserRedirectPage(string page, string cvalue, string value, int cartId, string ip, string oid, string bookingid, string read)
        {
            if (string.IsNullOrEmpty(value) || string.IsNullOrEmpty(cvalue) || cartId < 1 || string.IsNullOrEmpty(page))
            {
                return RedirectToAction("Error");
            }
            QueryStringValues model = new QueryStringValues();
            model.CustomerGUID = cvalue;
            model.BookingSittingGUID = value;
            model.CartID = cartId.ToString();
            model.BookingID = bookingid;
            model.IsAdmin = true;
            model.OrderID = string.IsNullOrEmpty(oid) ? "-1" : oid;
            model.IsVisited = false;
            if (read == "true")
            {
                model.IsReadOnly = true;
            }

            bool resSaveCookie = SaveLinksParametrs(model);
            if (!resSaveCookie)
            {
                return RedirectToAction("PasserDetails", bookingid);
            }
            string actionName = page;
            return RedirectToAction(actionName, "Booking");
        }
    }
}