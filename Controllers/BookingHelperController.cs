using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VirtualPass.CE4.Services;
using System.IO;

namespace VirtualPass.CE4.Controllers
{
    public class BookingHelperController : Controller
    {
        IImageProvider imagesProvider;

        public BookingHelperController(IImageProvider imagesProvider) {
            this.imagesProvider = imagesProvider;
        }

        public async Task<IActionResult> Images(string bookingID, string fileName) {
            Stream result = await imagesProvider.Download(bookingID, fileName);
            result.Position = 0;
            return base.File(result, "image/jpeg");
        }
        
        public async Task<IActionResult> ImagesOriginal(string bookingID, string fileName)
        {
            Stream result = await imagesProvider.Download(bookingID, fileName);
            result.Position = 0;
            return base.File(result, "image/jpeg");
        }
        public async Task<IActionResult> DigitalPack(string fileName)
        {
            Stream result = await imagesProvider.DownloadExactlyFrom(fileName);
            result.Position = 0;
            return base.File(result, "application/zip",Path.GetFileName(fileName));
        }

        public async Task<IActionResult> ImagesPreview(string bookingID, string fileName, int media = 150)
        {
            Stream result = await imagesProvider.DownloadPreview(bookingID, fileName,media);
            if (result == null)
            {
                result = await imagesProvider.Download("default.jpg");
                return base.File(result, "image/jpeg");
            }
            result.Position = 0;

            return base.File(result, "image/jpeg");
        }
    }
}