﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;

using VirtualPass.CE4.Core;
using VirtualPass.CE4.Data;
using VirtualPass.CE4.Models;
using VirtualPass.CE4.Services;
using VirtualPass.Core.Reps;
using VirtualPass.Token;
using NewCoreDll;
using Microsoft.AspNetCore.Identity;
using VirtualPass.CE4.Helpers;

namespace VirtualPass.CE4
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);


            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
               
            services.AddDbContext<DigitalCEContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DigitalCEConnection")));
            services.AddTransient<IUserValidator<ApplicationUser>, CustomUserValidator>();
            services.AddIdentity<ApplicationUser, IdentityRole>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 0;
                options.Password.RequireNonAlphanumeric = false;
                options.Cookies.ApplicationCookie.LoginPath = "/PasserAdmin/Login";
                options.Cookies.ApplicationCookie.ReturnUrlParameter = "";
                options.Lockout.DefaultLockoutTimeSpan = System.TimeSpan.FromMinutes(3);
                options.Cookies.ApplicationCookie.ExpireTimeSpan = System.TimeSpan.FromDays(150);
                options.Cookies.ApplicationCookie.LogoutPath = "/Account/LogOut";
            })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();
            
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            #region bearer
            services.AddAuthorization(auth =>
            {
                auth.AddPolicy("Bearer", new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme‌​)
                    .RequireAuthenticatedUser().Build());
            });
            #endregion bearer

            services.AddMvc();

            services.AddTransient<IStorage, AWSS3>();
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();
            services.AddTransient<IRepository, Repository>();
            services.AddTransient<IImageProvider, ImageProvider>();

			services.AddSingleton(provider => Configuration);
            services.AddScoped<AuthorizeIPAddressAttribute>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            #region bearer
            var tw =
                new TokenValidationParameters()
                {
                    IssuerSigningKey = TokenAuthOption.Key,
                    ValidAudience = TokenAuthOption.Audience,
                    ValidIssuer = TokenAuthOption.Issuer,
                    ValidateIssuerSigningKey = false,
                    ValidateLifetime = false,
                    ClockSkew = System.TimeSpan.FromMinutes(0)
                };
            app.UseJwtBearerAuthentication(new JwtBearerOptions()
            {
                TokenValidationParameters = tw
            });

            #endregion bearer
            app.UseStaticFiles();

            app.UseIdentity();

            // Add external authentication middleware below. To configure them please see http://go.microsoft.com/fwlink/?LinkID=532715

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=PasserAdmin}/{action=Index}/{id?}");
            });
         }
    }
}
